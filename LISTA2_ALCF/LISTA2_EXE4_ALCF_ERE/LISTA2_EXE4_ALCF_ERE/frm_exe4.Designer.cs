﻿namespace LISTA2_EXE4_ALCF_ERE
{
    partial class frm_exe4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_nvenda = new System.Windows.Forms.Button();
            this.btn_finalizar = new System.Windows.Forms.Button();
            this.lbl_resultado3 = new System.Windows.Forms.Label();
            this.lbl_resultado2 = new System.Windows.Forms.Label();
            this.lbl_resultado = new System.Windows.Forms.Label();
            this.btn_ok = new System.Windows.Forms.Button();
            this.txt_quantidade = new System.Windows.Forms.TextBox();
            this.lbl_quantidade = new System.Windows.Forms.Label();
            this.txt_preco = new System.Windows.Forms.TextBox();
            this.lbl_preco = new System.Windows.Forms.Label();
            this.txt_produto = new System.Windows.Forms.TextBox();
            this.lbl_produto = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_nvenda
            // 
            this.btn_nvenda.Location = new System.Drawing.Point(25, 257);
            this.btn_nvenda.Name = "btn_nvenda";
            this.btn_nvenda.Size = new System.Drawing.Size(97, 23);
            this.btn_nvenda.TabIndex = 23;
            this.btn_nvenda.Text = "Nova Venda";
            this.btn_nvenda.UseVisualStyleBackColor = true;
            this.btn_nvenda.Click += new System.EventHandler(this.btn_nvenda_Click);
            // 
            // btn_finalizar
            // 
            this.btn_finalizar.Location = new System.Drawing.Point(204, 257);
            this.btn_finalizar.Name = "btn_finalizar";
            this.btn_finalizar.Size = new System.Drawing.Size(75, 23);
            this.btn_finalizar.TabIndex = 22;
            this.btn_finalizar.Text = "Finalizar";
            this.btn_finalizar.UseVisualStyleBackColor = true;
            this.btn_finalizar.Click += new System.EventHandler(this.btn_finalizar_Click);
            // 
            // lbl_resultado3
            // 
            this.lbl_resultado3.AutoSize = true;
            this.lbl_resultado3.Location = new System.Drawing.Point(63, 345);
            this.lbl_resultado3.Name = "lbl_resultado3";
            this.lbl_resultado3.Size = new System.Drawing.Size(0, 13);
            this.lbl_resultado3.TabIndex = 21;
            // 
            // lbl_resultado2
            // 
            this.lbl_resultado2.AutoSize = true;
            this.lbl_resultado2.Location = new System.Drawing.Point(63, 329);
            this.lbl_resultado2.Name = "lbl_resultado2";
            this.lbl_resultado2.Size = new System.Drawing.Size(0, 13);
            this.lbl_resultado2.TabIndex = 20;
            // 
            // lbl_resultado
            // 
            this.lbl_resultado.AutoSize = true;
            this.lbl_resultado.Location = new System.Drawing.Point(63, 313);
            this.lbl_resultado.Name = "lbl_resultado";
            this.lbl_resultado.Size = new System.Drawing.Size(0, 13);
            this.lbl_resultado.TabIndex = 19;
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(122, 196);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 18;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // txt_quantidade
            // 
            this.txt_quantidade.Location = new System.Drawing.Point(107, 159);
            this.txt_quantidade.MaxLength = 3;
            this.txt_quantidade.Name = "txt_quantidade";
            this.txt_quantidade.Size = new System.Drawing.Size(100, 20);
            this.txt_quantidade.TabIndex = 17;
            // 
            // lbl_quantidade
            // 
            this.lbl_quantidade.AutoSize = true;
            this.lbl_quantidade.Location = new System.Drawing.Point(13, 162);
            this.lbl_quantidade.Name = "lbl_quantidade";
            this.lbl_quantidade.Size = new System.Drawing.Size(65, 13);
            this.lbl_quantidade.TabIndex = 16;
            this.lbl_quantidade.Text = "Quantidade:";
            // 
            // txt_preco
            // 
            this.txt_preco.Location = new System.Drawing.Point(107, 112);
            this.txt_preco.MaxLength = 6;
            this.txt_preco.Name = "txt_preco";
            this.txt_preco.Size = new System.Drawing.Size(90, 20);
            this.txt_preco.TabIndex = 15;
            // 
            // lbl_preco
            // 
            this.lbl_preco.AutoSize = true;
            this.lbl_preco.Location = new System.Drawing.Point(46, 115);
            this.lbl_preco.Name = "lbl_preco";
            this.lbl_preco.Size = new System.Drawing.Size(38, 13);
            this.lbl_preco.TabIndex = 14;
            this.lbl_preco.Text = "Preço:";
            // 
            // txt_produto
            // 
            this.txt_produto.Location = new System.Drawing.Point(107, 29);
            this.txt_produto.MaxLength = 140;
            this.txt_produto.Multiline = true;
            this.txt_produto.Name = "txt_produto";
            this.txt_produto.Size = new System.Drawing.Size(181, 59);
            this.txt_produto.TabIndex = 13;
            // 
            // lbl_produto
            // 
            this.lbl_produto.AutoSize = true;
            this.lbl_produto.Location = new System.Drawing.Point(43, 32);
            this.lbl_produto.Name = "lbl_produto";
            this.lbl_produto.Size = new System.Drawing.Size(47, 13);
            this.lbl_produto.TabIndex = 12;
            this.lbl_produto.Text = "Produto:";
            // 
            // frm_exe4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(304, 446);
            this.ControlBox = false;
            this.Controls.Add(this.btn_nvenda);
            this.Controls.Add(this.btn_finalizar);
            this.Controls.Add(this.lbl_resultado3);
            this.Controls.Add(this.lbl_resultado2);
            this.Controls.Add(this.lbl_resultado);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.txt_quantidade);
            this.Controls.Add(this.lbl_quantidade);
            this.Controls.Add(this.txt_preco);
            this.Controls.Add(this.lbl_preco);
            this.Controls.Add(this.txt_produto);
            this.Controls.Add(this.lbl_produto);
            this.Name = "frm_exe4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Orçamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_nvenda;
        private System.Windows.Forms.Button btn_finalizar;
        private System.Windows.Forms.Label lbl_resultado3;
        private System.Windows.Forms.Label lbl_resultado2;
        private System.Windows.Forms.Label lbl_resultado;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox txt_quantidade;
        private System.Windows.Forms.Label lbl_quantidade;
        private System.Windows.Forms.TextBox txt_preco;
        private System.Windows.Forms.Label lbl_preco;
        private System.Windows.Forms.TextBox txt_produto;
        private System.Windows.Forms.Label lbl_produto;
    }
}

