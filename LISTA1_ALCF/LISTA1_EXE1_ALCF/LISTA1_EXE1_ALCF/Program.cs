﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lista1
{
    class Program
    {
        static void Main(string[] args)
        {
            float nota =0;
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Digite a {0}° Nota: ", (i + 1));
                nota += float.Parse(Console.ReadLine());
            }
            nota = nota / 4;
            Console.WriteLine("A media é: {0}",nota);
            Console.ReadKey();
        }
    }
}
