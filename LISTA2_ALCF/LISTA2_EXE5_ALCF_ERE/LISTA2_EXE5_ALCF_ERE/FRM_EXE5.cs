﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LISTA2_EXE5_ALCF_ERE
{
    public partial class FRM_EXE5 : Form
    {
        public FRM_EXE5()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int largAE, compAE, areaAE;
            largAE = int.Parse(txt_largura.Text);
            compAE = int.Parse(txt_comprimento.Text);
            areaAE = largAE * compAE;
            MessageBox.Show(null,areaAE.ToString()+" M²","Area",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
