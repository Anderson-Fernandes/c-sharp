﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LISTA2_EXE2_ALCF_ERE
{
    public partial class frm_livaria : Form
    {
        public frm_livaria()
        {
            InitializeComponent();
            cmb_categoria.SelectedIndex = 0;
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (cmb_categoria.SelectedIndex == 0)
            {
                MessageBox.Show("Selecione uma categoria!");
            }
            else if (txt_livro.Text == "" || txt_livro.Text == null || txt_autor.Text == "" || txt_autor.Text == null || txt_valor.Text == null || txt_valor.Text == "")
            {
                 MessageBox.Show("Preencha todos os campos!");
            }
            else if (rdb_pagamento1.Checked == false && rdb_pagamento2.Checked == false)
            {
                MessageBox.Show("Selecione um tipo de pagamento!");
            }
            else
            {
                double valorAE, descontoAE;
                valorAE = double.Parse(txt_valor.Text);
                descontoAE = 0.2 * valorAE;
                descontoAE = valorAE - descontoAE;
                lbl_resultado.Text = "O Livro " + txt_livro.Text + " do Autor " + txt_autor.Text + "\n custa R$"
                    + txt_valor.Text + ", e à vista o livro custará " + descontoAE.ToString("C");
            }
        }

    }
}