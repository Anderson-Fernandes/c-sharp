﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExemploAula07
{
    public partial class Exemplo : Form
    {
        string nome,nomec;
        int posicao;

        public Exemplo()
        {
            InitializeComponent();
        }

        private void Exemplo_Load(object sender, EventArgs e)
        {
            lst_nome.Items.Add("Rubervinia");
            lst_nome.Items.Add("Virgulina");
            lst_nome.Items.Add("Zovaliana");
            cmb_nome.Items.Add("Abundensem");
            cmb_nome.Items.Add("Gotaskaim");

        }

        private void btn_excluirlist_Click(object sender, EventArgs e)
        {
            lst_nome.Items.Remove(nome);
            //lst_nome.Items.RemoveAt(posicao);
        }

        private void lst_nome_SelectedIndexChanged(object sender, EventArgs e)
        {
            nome = Convert.ToString(lst_nome.SelectedItem);
            //posicao = lst_nome.SelectedIndex;
        }

        private void cmb_nome_SelectedIndexChanged(object sender, EventArgs e)
        {
            nomec = Convert.ToString(cmb_nome.SelectedItem);
            // posicao = cmb.nome.SelectedIndex;
        }

        private void btn_excluircombo_Click(object sender, EventArgs e)
        {
            cmb_nome.Items.Remove(nomec);
            // posicao = cmb_nome.SelectedIndex;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
                {
                    if (rdb_List.Checked == true)
                    {
                      lst_nome.Items.Add(txt_nome.Text);
                     }
                    if(rdb_combo.Checked == true)
                    {
                     cmb_nome.Items.Add(txt_nome.Text);
                    }
                }
                catch
                {
                }
            }
    }
}
