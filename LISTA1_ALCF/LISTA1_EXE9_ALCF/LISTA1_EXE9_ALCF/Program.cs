﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LISTA1_EXE9_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean trueFalse = true;
            Boolean trueFalse2 = true;
            while (trueFalse)
            {
                Console.WriteLine("Informe a ano que deseja verificar: ");
                int mesA = int.Parse(Console.ReadLine());
                if (mesA % 4 == 0 && mesA % 100 != 0 || mesA % 400 == 0)
                {
                    Console.WriteLine("O Ano: {0}, é bissexto", mesA);
                }
                else
                {
                    Console.WriteLine("O Ano: {0}, não é bissexto", mesA);
                }
                while (trueFalse2)
                {
                    Console.WriteLine("Deseja repetir");
                    String answer = Console.ReadLine();
                    answer = answer.ToLower();
                    if (answer == "sim" || answer == "s")
                    {
                        Console.Clear();
                        trueFalse2 = false;
                    }
                    else if (answer == "não" || answer == "n" || answer == "nao")
                    {
                        trueFalse = false;
                        trueFalse2 = false;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Não consegui entender, Digite Sim ou Não");
                    }
                }
            }
        }
    }
}
