﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE3_ALCF_ERE
{
    public partial class frm_exe3 : Form
    {
        public frm_exe3()
        {
            InitializeComponent();
        }

        private void btn_tipo_Click(object sender, EventArgs e)
        {
            if (txt_a.TextLength == 0 || txt_b.TextLength == 0 || txt_c.TextLength == 0)
            {
                MessageBox.Show("Preencha todos os campos", "Aviso!", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    float aAE = conF(txt_a.Text);
                    float bAE = conF(txt_b.Text);
                    float cAE = conF(txt_c.Text);
                    String resposta = tipoTriangulo(aAE, bAE, cAE);
                    lbl_tipo.Text = resposta;
                }
                catch(FormatException)
                {
                    MessageBox.Show("Digite apenas numeros", "Aviso!", MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                catch(OverflowException)
                {
                    MessageBox.Show("Digite um numero menor", "Aviso!", MessageBoxButtons.OK,MessageBoxIcon.Error);
                }

            }
        }

        public String tipoTriangulo(float aAE, float bAE, float cAE)
        {
           float ladoAAE = aAE;
           float ladoBAE = bAE;
           float ladoCAE = cAE;
           String tipoTriangulo = "";
            if(ladoAAE == ladoBAE && ladoBAE == ladoCAE)
            {
                 tipoTriangulo= "Tipo: Equilátero";
            }
            else if(ladoAAE == ladoBAE || ladoBAE == ladoCAE)
            {
                tipoTriangulo = "Tipo: Isósceles";
            }
            else
            {
                tipoTriangulo = "Tipo: Escaleno";
            }
            return tipoTriangulo;
        }

        public float conF(String n1AE)
        {
            float conF = float.Parse(n1AE);
            return conF;
        }

    }
}
