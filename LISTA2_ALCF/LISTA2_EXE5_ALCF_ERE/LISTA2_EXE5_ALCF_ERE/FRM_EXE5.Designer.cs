﻿namespace LISTA2_EXE5_ALCF_ERE
{
    partial class FRM_EXE5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_largura = new System.Windows.Forms.Label();
            this.lbl_comprimento = new System.Windows.Forms.Label();
            this.txt_largura = new System.Windows.Forms.TextBox();
            this.txt_comprimento = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_largura
            // 
            this.lbl_largura.AutoSize = true;
            this.lbl_largura.Location = new System.Drawing.Point(13, 13);
            this.lbl_largura.Name = "lbl_largura";
            this.lbl_largura.Size = new System.Drawing.Size(81, 13);
            this.lbl_largura.TabIndex = 0;
            this.lbl_largura.Text = "Digite a largura:";
            // 
            // lbl_comprimento
            // 
            this.lbl_comprimento.AutoSize = true;
            this.lbl_comprimento.Location = new System.Drawing.Point(16, 49);
            this.lbl_comprimento.Name = "lbl_comprimento";
            this.lbl_comprimento.Size = new System.Drawing.Size(109, 13);
            this.lbl_comprimento.TabIndex = 1;
            this.lbl_comprimento.Text = "Digite o comprimento:";
            // 
            // txt_largura
            // 
            this.txt_largura.Location = new System.Drawing.Point(157, 10);
            this.txt_largura.Name = "txt_largura";
            this.txt_largura.Size = new System.Drawing.Size(100, 20);
            this.txt_largura.TabIndex = 2;
            // 
            // txt_comprimento
            // 
            this.txt_comprimento.Location = new System.Drawing.Point(157, 49);
            this.txt_comprimento.Name = "txt_comprimento";
            this.txt_comprimento.Size = new System.Drawing.Size(100, 20);
            this.txt_comprimento.TabIndex = 3;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(107, 205);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 4;
            this.btn_calc.Text = "Calcular";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // FRM_EXE5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_comprimento);
            this.Controls.Add(this.txt_largura);
            this.Controls.Add(this.lbl_comprimento);
            this.Controls.Add(this.lbl_largura);
            this.Name = "FRM_EXE5";
            this.ShowIcon = false;
            this.Text = "Area";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_largura;
        private System.Windows.Forms.Label lbl_comprimento;
        private System.Windows.Forms.TextBox txt_largura;
        private System.Windows.Forms.TextBox txt_comprimento;
        private System.Windows.Forms.Button btn_calc;
    }
}

