﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE9_ALCF_ERE
{
    public partial class frm_exe9 : Form
    {
        public frm_exe9()
        {
            InitializeComponent();
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_calcular_Click(object sender, EventArgs e)
        {
            if (msk_hr_mn_entrada.TextLength < 1 || msk_hr_mn_saida.TextLength < 1)
            {
                MessageBox.Show("Preencha todos os campos", "ATENÇÃO", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    int qtd_horas, qtd_minutos, he, me, hs, ms, vl;
                    me = int.Parse(msk_hr_mn_entrada.Text);
                    he = int.Parse(msk_hr_mn_entrada.Text);
                    me = me % 100;
                    he = he / 100;
                    ms = int.Parse(msk_hr_mn_saida.Text);
                    hs = int.Parse(msk_hr_mn_saida.Text);
                    ms = ms % 100;
                    hs = hs / 100;

                    if (hs < he)
                    {
                        qtd_horas = he - hs * (-1);
                    }
                    else
                    {
                        qtd_horas = hs - he;
                    }

                    qtd_minutos = ms - me;

                    if (qtd_horas == 1)
                    {
                        vl = 4;
                        if (qtd_minutos > 0)
                        {
                            vl = 6;
                            lbl_resultado.Text = ("O valor a ser pago pelo estacionamento é:\n " + vl.ToString("C"));
                        }
                        lbl_resultado.Text = ("O valor a ser pago pelo estacionamento é:\n " + vl.ToString("C"));
                    }
                    else if (qtd_horas == 2)
                    {
                        vl = 6;
                        if (qtd_minutos > 0)
                        {
                            vl = vl + 1;
                            lbl_resultado.Text = ("O valor a ser pago pelo estacionamento é:\n " + vl.ToString("C"));
                        }
                        lbl_resultado.Text = ("O valor a ser pago pelo estacionamento é:\n " + vl.ToString("C"));
                    }
                    else if (qtd_horas > 2)
                    {
                        vl = 6 + (qtd_horas - 2);
                        if (qtd_minutos > 0)
                        {
                            vl = vl + 1;
                            lbl_resultado.Text = ("O valor a ser pago pelo estacionamento é:\n " + vl.ToString("C"));
                        }
                        lbl_resultado.Text = ("O valor a ser pago pelo estacionamento é:\n " + vl.ToString("C"));
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Digite apenas numeros", "ATENÇÃO", MessageBoxButtons.OK);
                }
                catch (OverflowException)
                {
                    MessageBox.Show("Digite um numero menor", "ATENÇÃO", MessageBoxButtons.OK);
                }
            }
        }
    }
}
