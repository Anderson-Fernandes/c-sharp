﻿namespace ExemploAula07
{
    partial class Exemplo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_nome = new System.Windows.Forms.Label();
            this.txt_nome = new System.Windows.Forms.TextBox();
            this.lbl_title = new System.Windows.Forms.Label();
            this.rdb_List = new System.Windows.Forms.RadioButton();
            this.rdb_combo = new System.Windows.Forms.RadioButton();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.cmb_nome = new System.Windows.Forms.ComboBox();
            this.btn_excluirlist = new System.Windows.Forms.Button();
            this.btn_excluircombo = new System.Windows.Forms.Button();
            this.btn_sair = new System.Windows.Forms.Button();
            this.lst_nome = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbl_nome
            // 
            this.lbl_nome.AutoSize = true;
            this.lbl_nome.Location = new System.Drawing.Point(48, 50);
            this.lbl_nome.Name = "lbl_nome";
            this.lbl_nome.Size = new System.Drawing.Size(38, 13);
            this.lbl_nome.TabIndex = 0;
            this.lbl_nome.Text = "Nome:";
            // 
            // txt_nome
            // 
            this.txt_nome.Location = new System.Drawing.Point(116, 50);
            this.txt_nome.Name = "txt_nome";
            this.txt_nome.Size = new System.Drawing.Size(236, 20);
            this.txt_nome.TabIndex = 1;
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbl_title.Location = new System.Drawing.Point(48, 21);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(107, 16);
            this.lbl_title.TabIndex = 2;
            this.lbl_title.Text = "Inclusão de Dados";
            // 
            // rdb_List
            // 
            this.rdb_List.AutoSize = true;
            this.rdb_List.Checked = true;
            this.rdb_List.Location = new System.Drawing.Point(72, 96);
            this.rdb_List.Name = "rdb_List";
            this.rdb_List.Size = new System.Drawing.Size(105, 17);
            this.rdb_List.TabIndex = 3;
            this.rdb_List.TabStop = true;
            this.rdb_List.Text = "Incluir no ListBox";
            this.rdb_List.UseVisualStyleBackColor = true;
            // 
            // rdb_combo
            // 
            this.rdb_combo.AutoSize = true;
            this.rdb_combo.Location = new System.Drawing.Point(206, 96);
            this.rdb_combo.Name = "rdb_combo";
            this.rdb_combo.Size = new System.Drawing.Size(122, 17);
            this.rdb_combo.TabIndex = 4;
            this.rdb_combo.Text = "Incluir no ComboBox";
            this.rdb_combo.UseVisualStyleBackColor = true;
            // 
            // btn_incluir
            // 
            this.btn_incluir.Location = new System.Drawing.Point(51, 139);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(301, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "Incluir";
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // cmb_nome
            // 
            this.cmb_nome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_nome.FormattingEnabled = true;
            this.cmb_nome.Items.AddRange(new object[] {
            "Hamfuy",
            "Curtytudu"});
            this.cmb_nome.Location = new System.Drawing.Point(206, 180);
            this.cmb_nome.Name = "cmb_nome";
            this.cmb_nome.Size = new System.Drawing.Size(146, 21);
            this.cmb_nome.TabIndex = 7;
            this.cmb_nome.SelectedIndexChanged += new System.EventHandler(this.cmb_nome_SelectedIndexChanged);
            // 
            // btn_excluirlist
            // 
            this.btn_excluirlist.Location = new System.Drawing.Point(51, 305);
            this.btn_excluirlist.Name = "btn_excluirlist";
            this.btn_excluirlist.Size = new System.Drawing.Size(144, 23);
            this.btn_excluirlist.TabIndex = 8;
            this.btn_excluirlist.Text = "Excluir da ListBox";
            this.btn_excluirlist.UseVisualStyleBackColor = true;
            this.btn_excluirlist.Click += new System.EventHandler(this.btn_excluirlist_Click);
            // 
            // btn_excluircombo
            // 
            this.btn_excluircombo.Location = new System.Drawing.Point(206, 304);
            this.btn_excluircombo.Name = "btn_excluircombo";
            this.btn_excluircombo.Size = new System.Drawing.Size(146, 23);
            this.btn_excluircombo.TabIndex = 9;
            this.btn_excluircombo.Text = "Excluir da ComboBox";
            this.btn_excluircombo.UseVisualStyleBackColor = true;
            this.btn_excluircombo.Click += new System.EventHandler(this.btn_excluircombo_Click);
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(51, 345);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(301, 23);
            this.btn_sair.TabIndex = 10;
            this.btn_sair.Text = "Sair";
            this.btn_sair.UseVisualStyleBackColor = true;
            // 
            // lst_nome
            // 
            this.lst_nome.FormattingEnabled = true;
            this.lst_nome.Items.AddRange(new object[] {
            "Anacleto",
            "Astrogilda",
            "Epaminondas",
            "Tiburcio"});
            this.lst_nome.Location = new System.Drawing.Point(51, 180);
            this.lst_nome.Name = "lst_nome";
            this.lst_nome.Size = new System.Drawing.Size(135, 108);
            this.lst_nome.TabIndex = 11;
            this.lst_nome.SelectedIndexChanged += new System.EventHandler(this.lst_nome_SelectedIndexChanged);
            // 
            // Exemplo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 404);
            this.Controls.Add(this.lst_nome);
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.btn_excluircombo);
            this.Controls.Add(this.btn_excluirlist);
            this.Controls.Add(this.cmb_nome);
            this.Controls.Add(this.btn_incluir);
            this.Controls.Add(this.rdb_combo);
            this.Controls.Add(this.rdb_List);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.txt_nome);
            this.Controls.Add(this.lbl_nome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Exemplo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DS1 - Aula 07";
            this.Load += new System.EventHandler(this.Exemplo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_nome;
        private System.Windows.Forms.TextBox txt_nome;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.RadioButton rdb_List;
        private System.Windows.Forms.RadioButton rdb_combo;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.ComboBox cmb_nome;
        private System.Windows.Forms.Button btn_excluirlist;
        private System.Windows.Forms.Button btn_excluircombo;
        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.ListBox lst_nome;
    }
}

