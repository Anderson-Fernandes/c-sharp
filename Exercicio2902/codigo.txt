using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace MapasDosEstados
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MySqlConnection conectaBanco = new MySqlConnection("server=localhost;user id=root;password=root;persistsecurityinfo=True;database=db_escola");
            MySqlCommand comandoLerDados = new MySqlCommand("select cd_estado, sg_estado from tb_estado", conectaBanco);
            DataTable tabelaDadosMemoria = new DataTable();
            MySqlDataAdapter adaptador = new MySqlDataAdapter();

            adaptador.SelectCommand = comandoLerDados;

            conectaBanco.Open();
            adaptador.Fill(tabelaDadosMemoria);
            conectaBanco.Close();

            comboBox1.DataSource = tabelaDadosMemoria;

            comboBox1.ValueMember = "sg_estado";
            comboBox1.DisplayMember = "sg_estado";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlConnection conectaBanco = new MySqlConnection("server=localhost;user id=root;password=root;persistsecurityinfo=True;database=db_escola");
            MySqlCommand comandoLerDados = new MySqlCommand();
            MySqlDataReader dr;
            comandoLerDados.Connection = conectaBanco;
            comandoLerDados.CommandText = "select cd_estado, sg_estado, nm_estado from tb_estado WHERE sg_estado = '"+comboBox1.SelectedValue+"'";
            conectaBanco.Open();
            dr = comandoLerDados.ExecuteReader();
            while (dr.Read()){
                textBox1.Text = dr.GetString(2);
            }
            conectaBanco.Close();
        }
    }
 }
