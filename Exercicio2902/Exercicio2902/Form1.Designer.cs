﻿namespace Exercicio2902
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Estados = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_Estados = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.db_estadosDataSet = new Exercicio2902.db_estadosDataSet();
            this.tbestadoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tb_estadoTableAdapter = new Exercicio2902.db_estadosDataSetTableAdapters.tb_estadoTableAdapter();
            this.nmestadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nmsiglaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_estadosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbestadoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Estado";
            // 
            // txt_Estados
            // 
            this.txt_Estados.Location = new System.Drawing.Point(22, 25);
            this.txt_Estados.Name = "txt_Estados";
            this.txt_Estados.Size = new System.Drawing.Size(202, 20);
            this.txt_Estados.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Siglas";
            // 
            // cmb_Estados
            // 
            this.cmb_Estados.FormattingEnabled = true;
            this.cmb_Estados.Location = new System.Drawing.Point(22, 80);
            this.cmb_Estados.Name = "cmb_Estados";
            this.cmb_Estados.Size = new System.Drawing.Size(121, 21);
            this.cmb_Estados.TabIndex = 3;
            this.cmb_Estados.SelectedIndexChanged += new System.EventHandler(this.cmb_Estados_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nmestadoDataGridViewTextBoxColumn,
            this.nmsiglaDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tbestadoBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 126);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 111);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // db_estadosDataSet
            // 
            this.db_estadosDataSet.DataSetName = "db_estadosDataSet";
            this.db_estadosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbestadoBindingSource
            // 
            this.tbestadoBindingSource.DataMember = "tb_estado";
            this.tbestadoBindingSource.DataSource = this.db_estadosDataSet;
            // 
            // tb_estadoTableAdapter
            // 
            this.tb_estadoTableAdapter.ClearBeforeFill = true;
            // 
            // nmestadoDataGridViewTextBoxColumn
            // 
            this.nmestadoDataGridViewTextBoxColumn.DataPropertyName = "nm_estado";
            this.nmestadoDataGridViewTextBoxColumn.HeaderText = "nm_estado";
            this.nmestadoDataGridViewTextBoxColumn.Name = "nmestadoDataGridViewTextBoxColumn";
            // 
            // nmsiglaDataGridViewTextBoxColumn
            // 
            this.nmsiglaDataGridViewTextBoxColumn.DataPropertyName = "nm_sigla";
            this.nmsiglaDataGridViewTextBoxColumn.HeaderText = "nm_sigla";
            this.nmsiglaDataGridViewTextBoxColumn.Name = "nmsiglaDataGridViewTextBoxColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmb_Estados);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_Estados);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_estadosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbestadoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Estados;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmb_Estados;
        private System.Windows.Forms.DataGridView dataGridView1;
        private db_estadosDataSet db_estadosDataSet;
        private System.Windows.Forms.BindingSource tbestadoBindingSource;
        private db_estadosDataSetTableAdapters.tb_estadoTableAdapter tb_estadoTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmestadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmsiglaDataGridViewTextBoxColumn;
    }
}

