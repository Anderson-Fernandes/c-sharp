﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA2_EXE12_ALCF_ERE
{
    public partial class frm_exe12 : Form
    {
        public frm_exe12()
        {
            InitializeComponent();
        }

        private void btn_calcular_Click(object sender, EventArgs e)
        {
            double salario, novo;
            salario = double.Parse(txt_salario.Text);
            if (salario < 500)
            {
                novo = 0.15 * salario;
                novo = salario + novo;
                lbl_novo.Text = "Salário com reajuste: " + novo.ToString("C");
            }
            else
            {
                if (salario >= 500 && salario <= 1000)
                {
                    novo = 0.1 * salario;
                    novo = salario + novo;
                    lbl_novo.Text = "Salário com reajuste: " + novo.ToString("C"); ;
                }
                else
                {
                    if (salario > 1000)
                    {
                        novo = 0.05 * salario;
                        novo = salario + novo;
                        lbl_novo.Text = "Salário com reajuste: " + novo.ToString("C"); ;
                    }
                }
            }
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
