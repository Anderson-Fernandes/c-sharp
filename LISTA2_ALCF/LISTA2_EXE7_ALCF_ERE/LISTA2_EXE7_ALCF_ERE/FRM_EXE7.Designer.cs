﻿namespace LISTA2_EXE7_ALCF_ERE
{
    partial class FRM_EXE7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_calc = new System.Windows.Forms.Button();
            this.txt_desc = new System.Windows.Forms.TextBox();
            this.txt_valor = new System.Windows.Forms.TextBox();
            this.lbl_desconto = new System.Windows.Forms.Label();
            this.lbl_valor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_calc
            // 
            this.txt_calc.Location = new System.Drawing.Point(102, 200);
            this.txt_calc.Name = "txt_calc";
            this.txt_calc.Size = new System.Drawing.Size(94, 24);
            this.txt_calc.TabIndex = 4;
            this.txt_calc.Text = "Calcular";
            this.txt_calc.UseVisualStyleBackColor = true;
            this.txt_calc.Click += new System.EventHandler(this.txt_calc_Click);
            // 
            // txt_desc
            // 
            this.txt_desc.Location = new System.Drawing.Point(172, 61);
            this.txt_desc.Name = "txt_desc";
            this.txt_desc.Size = new System.Drawing.Size(100, 20);
            this.txt_desc.TabIndex = 3;
            this.txt_desc.TextChanged += new System.EventHandler(this.txt_desc_TextChanged);
            // 
            // txt_valor
            // 
            this.txt_valor.Location = new System.Drawing.Point(172, 19);
            this.txt_valor.Name = "txt_valor";
            this.txt_valor.Size = new System.Drawing.Size(100, 20);
            this.txt_valor.TabIndex = 2;
            this.txt_valor.TextChanged += new System.EventHandler(this.txt_valor_TextChanged);
            // 
            // lbl_desconto
            // 
            this.lbl_desconto.AutoSize = true;
            this.lbl_desconto.Location = new System.Drawing.Point(12, 64);
            this.lbl_desconto.Name = "lbl_desconto";
            this.lbl_desconto.Size = new System.Drawing.Size(93, 13);
            this.lbl_desconto.TabIndex = 1;
            this.lbl_desconto.Text = "Digite o desconto:";
            this.lbl_desconto.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbl_valor
            // 
            this.lbl_valor.AutoSize = true;
            this.lbl_valor.Location = new System.Drawing.Point(12, 22);
            this.lbl_valor.Name = "lbl_valor";
            this.lbl_valor.Size = new System.Drawing.Size(126, 13);
            this.lbl_valor.TabIndex = 0;
            this.lbl_valor.Text = "Digite o valor do produto:";
            this.lbl_valor.Click += new System.EventHandler(this.lbl_valor_Click);
            // 
            // FRM_EXE7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txt_calc);
            this.Controls.Add(this.txt_desc);
            this.Controls.Add(this.txt_valor);
            this.Controls.Add(this.lbl_desconto);
            this.Controls.Add(this.lbl_valor);
            this.Name = "FRM_EXE7";
            this.Text = "Desconto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button txt_calc;
        private System.Windows.Forms.TextBox txt_desc;
        private System.Windows.Forms.TextBox txt_valor;
        private System.Windows.Forms.Label lbl_desconto;
        private System.Windows.Forms.Label lbl_valor;

    }
}

