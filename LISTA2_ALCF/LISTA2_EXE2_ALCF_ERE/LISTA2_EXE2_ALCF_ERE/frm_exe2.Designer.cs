﻿namespace LISTA2_EXE2_ALCF_ERE
{
    partial class frm_livaria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_sair = new System.Windows.Forms.Button();
            this.lbl_resultado = new System.Windows.Forms.Label();
            this.btn_ok = new System.Windows.Forms.Button();
            this.rdb_pagamento2 = new System.Windows.Forms.RadioButton();
            this.rdb_pagamento1 = new System.Windows.Forms.RadioButton();
            this.lbl_pagamento = new System.Windows.Forms.Label();
            this.chk_ebook = new System.Windows.Forms.CheckBox();
            this.cmb_categoria = new System.Windows.Forms.ComboBox();
            this.lbl_categoria = new System.Windows.Forms.Label();
            this.txt_valor = new System.Windows.Forms.TextBox();
            this.lbl_valor = new System.Windows.Forms.Label();
            this.txt_autor = new System.Windows.Forms.TextBox();
            this.lbl_autor = new System.Windows.Forms.Label();
            this.txt_livro = new System.Windows.Forms.TextBox();
            this.lbl_livro = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(30, 303);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(75, 23);
            this.btn_sair.TabIndex = 29;
            this.btn_sair.Text = "Sair";
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // lbl_resultado
            // 
            this.lbl_resultado.AutoSize = true;
            this.lbl_resultado.Location = new System.Drawing.Point(48, 353);
            this.lbl_resultado.Name = "lbl_resultado";
            this.lbl_resultado.Size = new System.Drawing.Size(0, 13);
            this.lbl_resultado.TabIndex = 28;
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(173, 303);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 27;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // rdb_pagamento2
            // 
            this.rdb_pagamento2.AutoSize = true;
            this.rdb_pagamento2.Location = new System.Drawing.Point(175, 248);
            this.rdb_pagamento2.Name = "rdb_pagamento2";
            this.rdb_pagamento2.Size = new System.Drawing.Size(73, 17);
            this.rdb_pagamento2.TabIndex = 26;
            this.rdb_pagamento2.TabStop = true;
            this.rdb_pagamento2.Text = "Parcelado";
            this.rdb_pagamento2.UseVisualStyleBackColor = true;
            // 
            // rdb_pagamento1
            // 
            this.rdb_pagamento1.AutoSize = true;
            this.rdb_pagamento1.Location = new System.Drawing.Point(51, 248);
            this.rdb_pagamento1.Name = "rdb_pagamento1";
            this.rdb_pagamento1.Size = new System.Drawing.Size(58, 17);
            this.rdb_pagamento1.TabIndex = 25;
            this.rdb_pagamento1.TabStop = true;
            this.rdb_pagamento1.Text = "À Vista";
            this.rdb_pagamento1.UseVisualStyleBackColor = true;
            // 
            // lbl_pagamento
            // 
            this.lbl_pagamento.AutoSize = true;
            this.lbl_pagamento.Location = new System.Drawing.Point(98, 214);
            this.lbl_pagamento.Name = "lbl_pagamento";
            this.lbl_pagamento.Size = new System.Drawing.Size(103, 13);
            this.lbl_pagamento.TabIndex = 24;
            this.lbl_pagamento.Text = "Tipo de Pagamento:";
            // 
            // chk_ebook
            // 
            this.chk_ebook.AutoSize = true;
            this.chk_ebook.Location = new System.Drawing.Point(101, 178);
            this.chk_ebook.Name = "chk_ebook";
            this.chk_ebook.Size = new System.Drawing.Size(84, 17);
            this.chk_ebook.TabIndex = 23;
            this.chk_ebook.Text = "Tem eBook ";
            this.chk_ebook.UseVisualStyleBackColor = true;
            // 
            // cmb_categoria
            // 
            this.cmb_categoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_categoria.FormattingEnabled = true;
            this.cmb_categoria.Items.AddRange(new object[] {
            "Selecione...",
            "Aventura",
            "Ação",
            "Fantasia",
            "Romance",
            "Terror",
            "Ficção Científica",
            "Infantil"});
            this.cmb_categoria.Location = new System.Drawing.Point(127, 133);
            this.cmb_categoria.Name = "cmb_categoria";
            this.cmb_categoria.Size = new System.Drawing.Size(121, 21);
            this.cmb_categoria.TabIndex = 22;
            // 
            // lbl_categoria
            // 
            this.lbl_categoria.AutoSize = true;
            this.lbl_categoria.Location = new System.Drawing.Point(4, 136);
            this.lbl_categoria.Name = "lbl_categoria";
            this.lbl_categoria.Size = new System.Drawing.Size(96, 13);
            this.lbl_categoria.TabIndex = 21;
            this.lbl_categoria.Text = "Categoria do Livro:";
            // 
            // txt_valor
            // 
            this.txt_valor.Location = new System.Drawing.Point(127, 99);
            this.txt_valor.MaxLength = 5;
            this.txt_valor.Name = "txt_valor";
            this.txt_valor.Size = new System.Drawing.Size(121, 20);
            this.txt_valor.TabIndex = 20;
            // 
            // lbl_valor
            // 
            this.lbl_valor.AutoSize = true;
            this.lbl_valor.Location = new System.Drawing.Point(30, 102);
            this.lbl_valor.Name = "lbl_valor";
            this.lbl_valor.Size = new System.Drawing.Size(73, 13);
            this.lbl_valor.TabIndex = 19;
            this.lbl_valor.Text = "Valor Unitário:";
            // 
            // txt_autor
            // 
            this.txt_autor.Location = new System.Drawing.Point(127, 67);
            this.txt_autor.MaxLength = 30;
            this.txt_autor.Name = "txt_autor";
            this.txt_autor.Size = new System.Drawing.Size(121, 20);
            this.txt_autor.TabIndex = 18;
            // 
            // lbl_autor
            // 
            this.lbl_autor.AutoSize = true;
            this.lbl_autor.Location = new System.Drawing.Point(22, 70);
            this.lbl_autor.Name = "lbl_autor";
            this.lbl_autor.Size = new System.Drawing.Size(81, 13);
            this.lbl_autor.TabIndex = 17;
            this.lbl_autor.Text = "Nome do Autor:";
            // 
            // txt_livro
            // 
            this.txt_livro.Location = new System.Drawing.Point(127, 32);
            this.txt_livro.MaxLength = 20;
            this.txt_livro.Name = "txt_livro";
            this.txt_livro.Size = new System.Drawing.Size(121, 20);
            this.txt_livro.TabIndex = 16;
            // 
            // lbl_livro
            // 
            this.lbl_livro.AutoSize = true;
            this.lbl_livro.Location = new System.Drawing.Point(26, 35);
            this.lbl_livro.Name = "lbl_livro";
            this.lbl_livro.Size = new System.Drawing.Size(79, 13);
            this.lbl_livro.TabIndex = 15;
            this.lbl_livro.Text = "Nome do Livro:";
            // 
            // frm_livaria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(288, 384);
            this.ControlBox = false;
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.lbl_resultado);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.rdb_pagamento2);
            this.Controls.Add(this.rdb_pagamento1);
            this.Controls.Add(this.lbl_pagamento);
            this.Controls.Add(this.chk_ebook);
            this.Controls.Add(this.cmb_categoria);
            this.Controls.Add(this.lbl_categoria);
            this.Controls.Add(this.txt_valor);
            this.Controls.Add(this.lbl_valor);
            this.Controls.Add(this.txt_autor);
            this.Controls.Add(this.lbl_autor);
            this.Controls.Add(this.txt_livro);
            this.Controls.Add(this.lbl_livro);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_livaria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Livraria Ipanema";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Label lbl_resultado;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.RadioButton rdb_pagamento2;
        private System.Windows.Forms.RadioButton rdb_pagamento1;
        private System.Windows.Forms.Label lbl_pagamento;
        private System.Windows.Forms.CheckBox chk_ebook;
        private System.Windows.Forms.ComboBox cmb_categoria;
        private System.Windows.Forms.Label lbl_categoria;
        private System.Windows.Forms.TextBox txt_valor;
        private System.Windows.Forms.Label lbl_valor;
        private System.Windows.Forms.TextBox txt_autor;
        private System.Windows.Forms.Label lbl_autor;
        private System.Windows.Forms.TextBox txt_livro;
        private System.Windows.Forms.Label lbl_livro;
    }
}

