﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LISTA1_EXE7_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            //(a2b9c7)
            Console.WriteLine("Entre com o valor 'a': ");
            double aA = double.Parse(Console.ReadLine());
            if (aA != 0)
            {
                Console.WriteLine("Entre com o valor 'b': ");
                double bA = double.Parse(Console.ReadLine());
                Console.WriteLine("Entre com o valor 'c': ");
                double cA = double.Parse(Console.ReadLine());
                cA = Math.Pow(bA, 2) - 4 * aA * cA;
                if (cA < 0)
                {
                    Console.WriteLine("Não existem raízes reais");
                }
                else
                {
                    aA = Math.Sqrt(cA);
                    cA = ((bA * -1)- aA )/ 4;
                    bA = ((bA * -1) + aA) / 4;
                    Console.Clear();
                    Console.WriteLine("RESULTADO!!\nX1: {0}\nX2: {1}", bA, cA);
                }
            }
            else
            {
                Console.WriteLine("Não forma equação do 2º grau");
            }
            Console.ReadKey();

        }
    }
}
