﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA2_EXE6_ALCF_ERE
{
    public partial class fm_exe6 : Form
    {
        public fm_exe6()
        {
            InitializeComponent();
        }

        private void btn_desconto_Click(object sender, EventArgs e)
        {
            double preco, desconto;
            preco = double.Parse(txt_preco.Text);
            desconto = 0.05 * preco;
            lbl_desconto.Text = "O valor do desconto é: R$ " + desconto.ToString("C"); ;
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
