﻿namespace LISTA3_EXE6_ALCF_ERE
{
    partial class frm_exe6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_sair = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.txt_temperatura = new System.Windows.Forms.TextBox();
            this.lbl_termometro = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(13, 50);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(75, 23);
            this.btn_sair.TabIndex = 7;
            this.btn_sair.Text = "SAIR";
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.Location = new System.Drawing.Point(104, 50);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(75, 23);
            this.btn_OK.TabIndex = 6;
            this.btn_OK.Text = "OK";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // txt_temperatura
            // 
            this.txt_temperatura.Location = new System.Drawing.Point(86, 12);
            this.txt_temperatura.MaxLength = 4;
            this.txt_temperatura.Name = "txt_temperatura";
            this.txt_temperatura.Size = new System.Drawing.Size(93, 20);
            this.txt_temperatura.TabIndex = 5;
            // 
            // lbl_termometro
            // 
            this.lbl_termometro.AutoSize = true;
            this.lbl_termometro.Location = new System.Drawing.Point(13, 15);
            this.lbl_termometro.Name = "lbl_termometro";
            this.lbl_termometro.Size = new System.Drawing.Size(70, 13);
            this.lbl_termometro.TabIndex = 4;
            this.lbl_termometro.Text = "Temperatura:";
            // 
            // frm_exe6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 95);
            this.ControlBox = false;
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.txt_temperatura);
            this.Controls.Add(this.lbl_termometro);
            this.Name = "frm_exe6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Temperatura";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.TextBox txt_temperatura;
        private System.Windows.Forms.Label lbl_termometro;
    }
}

