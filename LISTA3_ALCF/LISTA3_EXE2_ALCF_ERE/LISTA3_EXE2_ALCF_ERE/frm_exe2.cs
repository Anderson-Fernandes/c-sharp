﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE2_ALCF_ERE
{
    public partial class frm_exe2 : Form
    {
        public frm_exe2()
        {
            InitializeComponent();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            txt_qtCaixa.Text = null;
            txt_qtProduto.Text = null;
            txt_vlProduto.Text = null;
        }

        private void bnt_enviar_Click(object sender, EventArgs e)
        {
            if (txt_qtCaixa.TextLength == 0 || txt_qtProduto.TextLength == 0 || txt_vlProduto.TextLength == 0)
            {
                MessageBox.Show("Preencha todos os campos!", "Atenção!", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    double caixaAE, quantidadeAE, precoAE, xAE, yAE, prazoAE, vistaAE;
                    caixaAE = double.Parse(txt_qtCaixa.Text);
                    quantidadeAE = double.Parse(txt_qtProduto.Text);
                    precoAE = double.Parse(txt_vlProduto.Text);

                    xAE = precoAE * quantidadeAE;
                    yAE = 0.8 * caixaAE;

                    if (xAE > yAE)
                    {
                        prazoAE = 0.1 * xAE;
                        prazoAE = xAE + prazoAE;
                        prazoAE = prazoAE / 3;
                        MessageBox.Show("A compra deverá ser realizada a prazo(3x) de " + prazoAE.ToString("C"),"Resultado",MessageBoxButtons.OK);
                    }
                    else
                    {
                        vistaAE = 0.05 * xAE;
                        vistaAE = xAE - vistaAE;
                        MessageBox.Show("A compra deverá ser realizada à vista de " + vistaAE.ToString("C"),"Resultado",MessageBoxButtons.OK);
                    }
                }
                catch(FormatException)
                {
                    MessageBox.Show("Digite apenas numeros!", "Atenção!", MessageBoxButtons.OK);
                }
                catch(OverflowException)
                {
                    MessageBox.Show("Digite um numero menor!", "Atenção", MessageBoxButtons.OK);
                }
            }

        }
    }
}
