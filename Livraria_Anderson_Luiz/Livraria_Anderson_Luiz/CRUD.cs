﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;


namespace Livraria_Anderson_Luiz
{
    class CRUD
    {
        private MySqlConnection cnx = new MySqlConnection("server=localhost;user id=root;password=root;database=db_livraria");
        private MySqlCommand cmd;
        private DataTable dt;
        private MySqlDataAdapter da;
        private MySqlDataReader dr;

        public MySqlDataReader select(String parametro, String tabela)
        {
            cnx.Open();
            cmd = new MySqlCommand("SELECT " + parametro + " FROM " + tabela, cnx);
            dt = new DataTable(tabela);
            dr = cmd.ExecuteReader();
            cnx.Close();
            return dr;
        }

        public MySqlDataReader select(String parametro, String tabela, String condicao)
        {        
          cmd = new MySqlCommand(" SELECT " + parametro + " FROM " + tabela + " "+ condicao, cnx);
            dt = new DataTable(tabela);
            dr = cmd.ExecuteReader();
            cnx.Close();
            return dr;
        }

        public String getRetornoString(MySqlDataReader dr,int i)
        {
            cnx.Open();
            String dados ="";
            while (dr.Read())
            {
                 dados = dr.GetString(i);
            }
            cnx.Close();
            return dados;

        }

        public void Open()
        {
            cnx.Open();
        }

        public void Close()
        {
            cnx.Close();
        }
    }


    
    
}
