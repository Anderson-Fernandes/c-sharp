﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE8_ALCF_ERE
{
    public partial class frm_exe8 : Form
    {
        public frm_exe8()
        {
            InitializeComponent();
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            int nascimento, dia, ano, mes, idade;
            nascimento = int.Parse(txt_nascimento.Text);
            ano = nascimento % 10000;
            nascimento = nascimento / 10000;
            mes = nascimento % 100;
            dia = nascimento / 100;

            idade = 2015 - ano;

            if (idade >= 5 && idade <= 7)
            {
                lbl_categoria.Text = "Categoria: Infantil A";
            }
            else
            {
                if (idade >= 8 && idade <= 10)
                {
                    lbl_categoria.Text = "Categoria: Infantil B";
                }
                else
                {
                    if (idade >= 11 && idade <= 13)
                    {
                        lbl_categoria.Text = "Categoria: Juvenil A";
                    }
                    else
                    {
                        if (idade >= 14 && idade <= 17)
                        {
                            lbl_categoria.Text = "Categoria: Juvenil B";
                        }
                        else
                        {
                            if (idade > 18)
                            {
                                lbl_categoria.Text = "Categoria: Adulto";
                            }
                        }
                    }
                }
            }
        }

        public void ERRO(String labelAE)
        {
            MessageBox.Show(labelAE,"ATENÇÃO",MessageBoxButtons.OK);
        }

        public void mens(String labelAE)
        {
            MessageBox.Show(labelAE,"Categoria",MessageBoxButtons.OK);
        }
    }
}
