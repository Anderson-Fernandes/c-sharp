﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE10_ALCF_ERE
{
    public partial class frm_exe10 : Form
    {
        public frm_exe10()
        {
            InitializeComponent();

        }
        int distanciaAE, kmiAE, kmfAE;
        double precoAE, litrosAE;
        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (txt_fkm.TextLength == 0 || txt_ikm.TextLength == 0 || txt_vlcombustivel.TextLength == 0 || rdb_alcool.Checked == false && rdb_gasolina.Checked == false)
            {
                MessageBox.Show("Todos os campos precisam estar preenchidos");
            }
            else
            {
                try
                {
                    precoAE = Double.Parse(txt_vlcombustivel.Text);
                    kmiAE = int.Parse(txt_ikm.Text);
                    kmfAE = int.Parse(txt_fkm.Text);
                    precoAE = Double.Parse(txt_vlcombustivel.Text);
                    distanciaAE = kmfAE - kmiAE;
                    if (rdb_alcool.Checked == true)
                    {
                        litrosAE = distanciaAE / 8;
                        precoAE = precoAE * litrosAE;
                        lbl_resultado.Text = "Voce gastou " + litrosAE.ToString() + " litros e " + precoAE.ToString("C");
                    }
                    else
                    {
                        if (rdb_gasolina.Checked == true)
                        {
                            litrosAE = distanciaAE / 10;
                            precoAE = precoAE * litrosAE;
                            lbl_resultado.Text = "Voce gastou " + litrosAE.ToString() + " litros e " + precoAE.ToString("C");
                        }
                    }
                }
                catch(FormatException)
                {
                    MessageBox.Show("Digite apenas numeros","ATENÇÃO",MessageBoxButtons.OK);
                }
                catch(OverflowException)
                {
                    MessageBox.Show("Digite um numero menor","ATENÇÃO",MessageBoxButtons.OK);
                }
            }
        }
    }
}
