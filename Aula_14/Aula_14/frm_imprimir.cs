﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Aula_14
{
    public partial class frm_imprimir : Form
    {
        //OpenFileDialog é o objeto que irá exibir uma caixa de dialogo para que possa ser aberto o arquivo de texto
        public OpenFileDialog abre_arquivo = new OpenFileDialog();
        //PrintDialog é o objeto que irá exibir uma caixa de dialogo para definir caracteristicas para a impressao
        public PrintDialog finaliza_impressao = new PrintDialog();
        private String texto;

        public frm_imprimir()
        {
            InitializeComponent();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //String que armazena o caminho do arquivo
            String caminho_arquivo;
            //Aqui é definido que tipo de aquivo será aberto/visualizado
            abre_arquivo.Filter = "Arquivo de texto (*.txt) | *.txt";
            //Exibe uma caixa de dialogo para a abertura de arquivos
            abre_arquivo.ShowDialog();


            if (abre_arquivo.FileName != "")
            {
                caminho_arquivo = abre_arquivo.FileName;
                FileStream manipula_arquivo = new FileStream(caminho_arquivo, FileMode.Open);

                try
                {
                    rtb_texto.LoadFile(manipula_arquivo, RichTextBoxStreamType.PlainText);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Erro ao tentar abrir o arquivo! Descrição: " + ex.Message, "Aviso",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }

                manipula_arquivo.Close();
            }
        }

        private void imprimirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Especifica o texto para a caixa de dialogo de impressao
                texto = rtb_texto.Text;
                finaliza_impressao.Document = imprime_arquivo;

                DialogResult result = finaliza_impressao.ShowDialog();
                // se clica em OK, imprime documento praa impressora

                if (result == DialogResult.OK)
                {
                    imprime_arquivo.Print();
                }
            }
            catch(Exception ex){
                MessageBox.Show("Erro ao tentar imprimir o arquivo! Descrição: " + ex.Message,"Aviso",MessageBoxButtons.OK,MessageBoxIcon.Error); 
            }
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


      

    }
}
