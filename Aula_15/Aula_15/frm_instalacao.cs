﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_15
{
    public partial class frm_instalacao : Form
    {
        public frm_instalacao()
        {
            InitializeComponent();
        }

        private void btn_exibir_Click(object sender, EventArgs e)
        {
            MessageBox.Show(txt_entrada.Text + " - Meu primeiro pacote de instalação!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
