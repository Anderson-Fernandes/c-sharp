﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LISTA1_EXE5_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            float transAE=0;
            bool tfAE = true;
            while (tfAE)
            {
                Console.Clear();
                Console.WriteLine("Entre com o primeiro numero: ");
                float Num1AE = float.Parse(Console.ReadLine());
                Console.WriteLine("\nEntre com o segundo numero: ");
                float Num2AE = float.Parse(Console.ReadLine());
                Console.WriteLine("\nEntre com o terceiro numero: ");
                float Num3AE = float.Parse(Console.ReadLine());
                if (Num1AE == Num2AE || Num2AE == Num3AE || Num1AE == Num3AE)
                {
                    Console.Clear();
                    Console.WriteLine("Não digite numeros Iguais!\nPrecione 'Enter'");
                    Console.ReadKey();
                }
                else
                {
                    while (tfAE)
                    {
                        if (Num1AE < Num2AE)
                        {
                            transAE = Num2AE;
                            Num2AE = Num1AE;
                            Num1AE = transAE;
                        }
                        else if (Num1AE < Num3AE)
                        {
                            transAE = Num3AE;
                            Num3AE = Num1AE;
                            Num1AE = transAE;

                        }
                        else if (Num2AE < Num3AE)
                        {
                            transAE = Num3AE;
                            Num3AE = Num2AE;
                            Num2AE = transAE;
                        }
                        else if (Num1AE > Num2AE && Num2AE > Num3AE)
                        {
                            tfAE = false;
                        }
                    }
                    Console.Clear();
                    Console.WriteLine("Ordem Crescente");
                    Console.WriteLine("\n{2}\n{1}\n{0}", Num1AE, Num2AE, Num3AE);
                    Console.ReadKey();
                }
                
            }
        }
    }
}
