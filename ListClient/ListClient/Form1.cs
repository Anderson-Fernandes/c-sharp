﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ListClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        conDB cnx = new conDB();
        private void Form1_Load(object sender, EventArgs e)
        {
            
            list_cliente.DataSource = cnx.select("*","tb_cliente");
            list_cliente.DisplayMember = "nm_cliente";
         
          
           
            

        }

        private void list_cliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl_codigo.Text = (String)cnx.retorno(list_cliente, "cd_cliente");
            lbl_nome.Text = (String)cnx.retorno(list_cliente, "nm_cliente");
            lbl_celular.Text = (String)cnx.retorno(list_cliente, "nm_celular");
            lbl_email.Text = (String)cnx.retorno(list_cliente, "nm_email");

            /*list_cliente.ValueMember = "cd_cliente";
            lbl_codigo.Text = "" + list_cliente.SelectedValue;
            list_cliente.ValueMember = "nm_email";
            lbl_email.Text = "" + list_cliente.SelectedValue;
              *///  comandoLerDados.CommandText = "select cd_estado, sg_estado, nm_estado from tb_estado WHERE sg_estado = '" + comboBox1.SelectedValue + "'";
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
            list_cliente.DataSource = cnx.select("* ", "tb_cliente ", "nm_cliente LIKE '%" + txt_buscar.Text + "%'");
            list_cliente.DisplayMember = "nm_cliente";
            list_cliente.ValueMember = "cd_cliente";
        }
    }
}
