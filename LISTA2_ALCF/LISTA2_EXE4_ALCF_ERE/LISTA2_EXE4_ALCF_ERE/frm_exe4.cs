﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA2_EXE4_ALCF_ERE
{
    public partial class frm_exe4 : Form
    {
        public frm_exe4()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            double precoAE, qtAE, totalAE, avistaAE, parceladoAE, trintadiasAE;
            precoAE = double.Parse(txt_preco.Text);
            qtAE = double.Parse(txt_quantidade.Text);
            totalAE = qtAE * precoAE;
            totalAE.ToString("C");

            avistaAE = 0.2 * totalAE;
            avistaAE = totalAE - avistaAE;


            trintadiasAE = 0.05 * totalAE;
            trintadiasAE = totalAE - trintadiasAE;


            parceladoAE = totalAE / 4;


            lbl_resultado.Text = "Á vista com 20% de desconto: " + avistaAE.ToString("C");
            lbl_resultado2.Text = "30 dias com 5% de desconto: " + trintadiasAE.ToString("C");
            lbl_resultado3.Text = "Em até 4 pagamentos de: " + parceladoAE.ToString("C");
        }

        private void btn_finalizar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja finalizar a compra? ", "Finalizar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_nvenda_Click(object sender, EventArgs e)
        {
            txt_produto.Text = null;
            txt_preco.Text = null;
            txt_quantidade.Text = null;
            this.txt_produto.Focus();
        }
    }
}
