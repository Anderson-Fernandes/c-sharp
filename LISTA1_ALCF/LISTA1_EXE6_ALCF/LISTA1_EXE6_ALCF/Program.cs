﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LISTA1_EXE6_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            float transA=0;
            bool tfA = true;
            while (tfA)
            {
                Console.Clear();
                Console.WriteLine("Entre com o primeiro numero: ");
                float Num1A = float.Parse(Console.ReadLine());
                Console.WriteLine("\nEntre com o segundo numero: ");
                float Num2A = float.Parse(Console.ReadLine());
                Console.WriteLine("\nEntre com o terceiro numero: ");
                float Num3A = float.Parse(Console.ReadLine());
                if (Num1A == Num2A || Num2A == Num3A || Num1A == Num3A)
                {
                    Console.Clear();
                    Console.WriteLine("Não digite numeros Iguais!\nPrecione 'Enter'");
                    Console.ReadKey();
                }
                else
                {
                    while (tfA)
                    {
                        if (Num1A < Num2A)
                        {
                            transA = Num2A;
                            Num2A = Num1A;
                            Num1A = transA;
                        }
                        else if (Num1A < Num3A)
                        {
                            transA = Num3A;
                            Num3A = Num1A;
                            Num1A = transA;

                        }
                        else if (Num2A < Num3A)
                        {
                            transA = Num3A;
                            Num3A = Num2A;
                            Num2A = transA;
                        }
                        else if (Num1A > Num2A && Num2A > Num3A)
                        {
                            tfA = false;
                        }
                    }
                    Console.Clear();
                    Console.WriteLine("Ordem Decrescente");
                    Console.WriteLine("\n{0}\n{1}\n{2}", Num1A, Num2A, Num3A);
                    Console.ReadKey();
                }
                
            }
        }
    }
}