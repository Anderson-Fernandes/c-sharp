﻿namespace LISTA4_EXE1_ALCF_ERE
{
    partial class Frm_exe1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_nome = new System.Windows.Forms.Label();
            this.txt_cliente = new System.Windows.Forms.TextBox();
            this.grp_compra = new System.Windows.Forms.GroupBox();
            this.rdb_nao = new System.Windows.Forms.RadioButton();
            this.rdb_sim = new System.Windows.Forms.RadioButton();
            this.lbl_valor = new System.Windows.Forms.Label();
            this.txt_valor = new System.Windows.Forms.TextBox();
            this.grp_fatura = new System.Windows.Forms.GroupBox();
            this.rdb_cD = new System.Windows.Forms.RadioButton();
            this.rdb_tD = new System.Windows.Forms.RadioButton();
            this.rdb_avista = new System.Windows.Forms.RadioButton();
            this.grp_vendedor = new System.Windows.Forms.GroupBox();
            this.rdb_carlos = new System.Windows.Forms.RadioButton();
            this.rdb_maria = new System.Windows.Forms.RadioButton();
            this.rdb_ernesto = new System.Windows.Forms.RadioButton();
            this.rdb_joaquim = new System.Windows.Forms.RadioButton();
            this.btn_confirmar = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.grp_compra.SuspendLayout();
            this.grp_fatura.SuspendLayout();
            this.grp_vendedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_nome
            // 
            this.lbl_nome.AutoSize = true;
            this.lbl_nome.Location = new System.Drawing.Point(13, 13);
            this.lbl_nome.Name = "lbl_nome";
            this.lbl_nome.Size = new System.Drawing.Size(85, 13);
            this.lbl_nome.TabIndex = 0;
            this.lbl_nome.Text = "Nome do Cliente";
            // 
            // txt_cliente
            // 
            this.txt_cliente.Location = new System.Drawing.Point(16, 40);
            this.txt_cliente.Name = "txt_cliente";
            this.txt_cliente.Size = new System.Drawing.Size(248, 20);
            this.txt_cliente.TabIndex = 1;
            this.txt_cliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_cliente_KeyPress);
            // 
            // grp_compra
            // 
            this.grp_compra.Controls.Add(this.rdb_nao);
            this.grp_compra.Controls.Add(this.rdb_sim);
            this.grp_compra.Location = new System.Drawing.Point(16, 66);
            this.grp_compra.Name = "grp_compra";
            this.grp_compra.Size = new System.Drawing.Size(113, 49);
            this.grp_compra.TabIndex = 2;
            this.grp_compra.TabStop = false;
            this.grp_compra.Text = "Primeira Compra?";
            // 
            // rdb_nao
            // 
            this.rdb_nao.AutoSize = true;
            this.rdb_nao.Location = new System.Drawing.Point(55, 19);
            this.rdb_nao.Name = "rdb_nao";
            this.rdb_nao.Size = new System.Drawing.Size(45, 17);
            this.rdb_nao.TabIndex = 1;
            this.rdb_nao.TabStop = true;
            this.rdb_nao.Text = "Não";
            this.rdb_nao.UseVisualStyleBackColor = true;
            this.rdb_nao.CheckedChanged += new System.EventHandler(this.rdb_nao_CheckedChanged);
            // 
            // rdb_sim
            // 
            this.rdb_sim.AutoSize = true;
            this.rdb_sim.Location = new System.Drawing.Point(7, 19);
            this.rdb_sim.Name = "rdb_sim";
            this.rdb_sim.Size = new System.Drawing.Size(42, 17);
            this.rdb_sim.TabIndex = 0;
            this.rdb_sim.TabStop = true;
            this.rdb_sim.Text = "Sim";
            this.rdb_sim.UseVisualStyleBackColor = true;
            this.rdb_sim.CheckedChanged += new System.EventHandler(this.rdb_sim_CheckedChanged);
            // 
            // lbl_valor
            // 
            this.lbl_valor.AutoSize = true;
            this.lbl_valor.Location = new System.Drawing.Point(149, 66);
            this.lbl_valor.Name = "lbl_valor";
            this.lbl_valor.Size = new System.Drawing.Size(85, 13);
            this.lbl_valor.TabIndex = 3;
            this.lbl_valor.Text = "Valor da Compra";
            // 
            // txt_valor
            // 
            this.txt_valor.Location = new System.Drawing.Point(152, 85);
            this.txt_valor.Name = "txt_valor";
            this.txt_valor.Size = new System.Drawing.Size(112, 20);
            this.txt_valor.TabIndex = 4;
            this.txt_valor.Text = "0";
            this.txt_valor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_valor_KeyPress);
            this.txt_valor.Leave += new System.EventHandler(this.txt_valor_Leave);
            // 
            // grp_fatura
            // 
            this.grp_fatura.Controls.Add(this.rdb_cD);
            this.grp_fatura.Controls.Add(this.rdb_tD);
            this.grp_fatura.Controls.Add(this.rdb_avista);
            this.grp_fatura.Location = new System.Drawing.Point(16, 124);
            this.grp_fatura.Name = "grp_fatura";
            this.grp_fatura.Size = new System.Drawing.Size(103, 122);
            this.grp_fatura.TabIndex = 5;
            this.grp_fatura.TabStop = false;
            this.grp_fatura.Text = "Tipo de Fatura";
            // 
            // rdb_cD
            // 
            this.rdb_cD.AutoSize = true;
            this.rdb_cD.Location = new System.Drawing.Point(10, 80);
            this.rdb_cD.Name = "rdb_cD";
            this.rdb_cD.Size = new System.Drawing.Size(59, 17);
            this.rdb_cD.TabIndex = 2;
            this.rdb_cD.TabStop = true;
            this.rdb_cD.Text = "60 dias";
            this.rdb_cD.UseVisualStyleBackColor = true;
            // 
            // rdb_tD
            // 
            this.rdb_tD.AutoSize = true;
            this.rdb_tD.Location = new System.Drawing.Point(10, 55);
            this.rdb_tD.Name = "rdb_tD";
            this.rdb_tD.Size = new System.Drawing.Size(59, 17);
            this.rdb_tD.TabIndex = 1;
            this.rdb_tD.TabStop = true;
            this.rdb_tD.Text = "30 dias";
            this.rdb_tD.UseVisualStyleBackColor = true;
            // 
            // rdb_avista
            // 
            this.rdb_avista.AutoSize = true;
            this.rdb_avista.Location = new System.Drawing.Point(10, 30);
            this.rdb_avista.Name = "rdb_avista";
            this.rdb_avista.Size = new System.Drawing.Size(57, 17);
            this.rdb_avista.TabIndex = 0;
            this.rdb_avista.TabStop = true;
            this.rdb_avista.Text = "A vista";
            this.rdb_avista.UseVisualStyleBackColor = true;
            // 
            // grp_vendedor
            // 
            this.grp_vendedor.Controls.Add(this.rdb_carlos);
            this.grp_vendedor.Controls.Add(this.rdb_maria);
            this.grp_vendedor.Controls.Add(this.rdb_ernesto);
            this.grp_vendedor.Controls.Add(this.rdb_joaquim);
            this.grp_vendedor.Location = new System.Drawing.Point(146, 124);
            this.grp_vendedor.Name = "grp_vendedor";
            this.grp_vendedor.Size = new System.Drawing.Size(113, 122);
            this.grp_vendedor.TabIndex = 6;
            this.grp_vendedor.TabStop = false;
            this.grp_vendedor.Text = "Vendedor";
            // 
            // rdb_carlos
            // 
            this.rdb_carlos.AutoSize = true;
            this.rdb_carlos.Location = new System.Drawing.Point(11, 90);
            this.rdb_carlos.Name = "rdb_carlos";
            this.rdb_carlos.Size = new System.Drawing.Size(54, 17);
            this.rdb_carlos.TabIndex = 3;
            this.rdb_carlos.TabStop = true;
            this.rdb_carlos.Text = "Carlos";
            this.rdb_carlos.UseVisualStyleBackColor = true;
            // 
            // rdb_maria
            // 
            this.rdb_maria.AutoSize = true;
            this.rdb_maria.Location = new System.Drawing.Point(11, 65);
            this.rdb_maria.Name = "rdb_maria";
            this.rdb_maria.Size = new System.Drawing.Size(51, 17);
            this.rdb_maria.TabIndex = 2;
            this.rdb_maria.TabStop = true;
            this.rdb_maria.Text = "Maria";
            this.rdb_maria.UseVisualStyleBackColor = true;
            // 
            // rdb_ernesto
            // 
            this.rdb_ernesto.AutoSize = true;
            this.rdb_ernesto.Location = new System.Drawing.Point(11, 42);
            this.rdb_ernesto.Name = "rdb_ernesto";
            this.rdb_ernesto.Size = new System.Drawing.Size(61, 17);
            this.rdb_ernesto.TabIndex = 1;
            this.rdb_ernesto.TabStop = true;
            this.rdb_ernesto.Text = "Ernesto";
            this.rdb_ernesto.UseVisualStyleBackColor = true;
            // 
            // rdb_joaquim
            // 
            this.rdb_joaquim.AutoSize = true;
            this.rdb_joaquim.Location = new System.Drawing.Point(11, 19);
            this.rdb_joaquim.Name = "rdb_joaquim";
            this.rdb_joaquim.Size = new System.Drawing.Size(64, 17);
            this.rdb_joaquim.TabIndex = 0;
            this.rdb_joaquim.TabStop = true;
            this.rdb_joaquim.Text = "Joaquim";
            this.rdb_joaquim.UseVisualStyleBackColor = true;
            // 
            // btn_confirmar
            // 
            this.btn_confirmar.Location = new System.Drawing.Point(26, 252);
            this.btn_confirmar.Name = "btn_confirmar";
            this.btn_confirmar.Size = new System.Drawing.Size(75, 23);
            this.btn_confirmar.TabIndex = 7;
            this.btn_confirmar.Text = "Confirmar";
            this.btn_confirmar.UseVisualStyleBackColor = true;
            this.btn_confirmar.Click += new System.EventHandler(this.btn_confirmar_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.Location = new System.Drawing.Point(159, 252);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 8;
            this.btn_limpar.Text = "Limpar";
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // Frm_exe1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 294);
            this.Controls.Add(this.btn_limpar);
            this.Controls.Add(this.btn_confirmar);
            this.Controls.Add(this.grp_vendedor);
            this.Controls.Add(this.grp_fatura);
            this.Controls.Add(this.txt_valor);
            this.Controls.Add(this.lbl_valor);
            this.Controls.Add(this.grp_compra);
            this.Controls.Add(this.txt_cliente);
            this.Controls.Add(this.lbl_nome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Frm_exe1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Clientes";
            this.grp_compra.ResumeLayout(false);
            this.grp_compra.PerformLayout();
            this.grp_fatura.ResumeLayout(false);
            this.grp_fatura.PerformLayout();
            this.grp_vendedor.ResumeLayout(false);
            this.grp_vendedor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_nome;
        private System.Windows.Forms.TextBox txt_cliente;
        private System.Windows.Forms.GroupBox grp_compra;
        private System.Windows.Forms.RadioButton rdb_nao;
        private System.Windows.Forms.RadioButton rdb_sim;
        private System.Windows.Forms.Label lbl_valor;
        private System.Windows.Forms.TextBox txt_valor;
        private System.Windows.Forms.GroupBox grp_fatura;
        private System.Windows.Forms.RadioButton rdb_cD;
        private System.Windows.Forms.RadioButton rdb_tD;
        private System.Windows.Forms.RadioButton rdb_avista;
        private System.Windows.Forms.GroupBox grp_vendedor;
        private System.Windows.Forms.RadioButton rdb_carlos;
        private System.Windows.Forms.RadioButton rdb_maria;
        private System.Windows.Forms.RadioButton rdb_ernesto;
        private System.Windows.Forms.RadioButton rdb_joaquim;
        private System.Windows.Forms.Button btn_confirmar;
        private System.Windows.Forms.Button btn_limpar;
    }
}

