﻿namespace LISTA3_EXE9_ALCF_ERE
{
    partial class frm_exe9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_sair = new System.Windows.Forms.Button();
            this.btn_calcular = new System.Windows.Forms.Button();
            this.msk_hr_mn_saida = new System.Windows.Forms.MaskedTextBox();
            this.lbl_hr_mn_saida = new System.Windows.Forms.Label();
            this.msk_hr_mn_entrada = new System.Windows.Forms.MaskedTextBox();
            this.lbl_hr_mn_entrada = new System.Windows.Forms.Label();
            this.lbl_resultado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(37, 191);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(75, 23);
            this.btn_sair.TabIndex = 11;
            this.btn_sair.Text = "&Sair";
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // btn_calcular
            // 
            this.btn_calcular.Location = new System.Drawing.Point(175, 191);
            this.btn_calcular.Name = "btn_calcular";
            this.btn_calcular.Size = new System.Drawing.Size(75, 23);
            this.btn_calcular.TabIndex = 10;
            this.btn_calcular.Text = "Calcular";
            this.btn_calcular.UseVisualStyleBackColor = true;
            this.btn_calcular.Click += new System.EventHandler(this.btn_calcular_Click);
            // 
            // msk_hr_mn_saida
            // 
            this.msk_hr_mn_saida.Location = new System.Drawing.Point(192, 75);
            this.msk_hr_mn_saida.Mask = "90:00";
            this.msk_hr_mn_saida.Name = "msk_hr_mn_saida";
            this.msk_hr_mn_saida.Size = new System.Drawing.Size(58, 20);
            this.msk_hr_mn_saida.TabIndex = 9;
            this.msk_hr_mn_saida.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.msk_hr_mn_saida.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            this.msk_hr_mn_saida.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_hr_mn_saida
            // 
            this.lbl_hr_mn_saida.AutoSize = true;
            this.lbl_hr_mn_saida.Location = new System.Drawing.Point(34, 78);
            this.lbl_hr_mn_saida.Name = "lbl_hr_mn_saida";
            this.lbl_hr_mn_saida.Size = new System.Drawing.Size(113, 13);
            this.lbl_hr_mn_saida.TabIndex = 8;
            this.lbl_hr_mn_saida.Text = "Digite a hora de saida:";
            // 
            // msk_hr_mn_entrada
            // 
            this.msk_hr_mn_entrada.Location = new System.Drawing.Point(192, 32);
            this.msk_hr_mn_entrada.Mask = "90:00";
            this.msk_hr_mn_entrada.Name = "msk_hr_mn_entrada";
            this.msk_hr_mn_entrada.Size = new System.Drawing.Size(58, 20);
            this.msk_hr_mn_entrada.TabIndex = 7;
            this.msk_hr_mn_entrada.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.msk_hr_mn_entrada.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            this.msk_hr_mn_entrada.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_hr_mn_entrada
            // 
            this.lbl_hr_mn_entrada.AutoSize = true;
            this.lbl_hr_mn_entrada.Location = new System.Drawing.Point(34, 35);
            this.lbl_hr_mn_entrada.Name = "lbl_hr_mn_entrada";
            this.lbl_hr_mn_entrada.Size = new System.Drawing.Size(124, 13);
            this.lbl_hr_mn_entrada.TabIndex = 6;
            this.lbl_hr_mn_entrada.Text = "Digite a hora de entrada:";
            // 
            // lbl_resultado
            // 
            this.lbl_resultado.AutoSize = true;
            this.lbl_resultado.Location = new System.Drawing.Point(34, 128);
            this.lbl_resultado.Name = "lbl_resultado";
            this.lbl_resultado.Size = new System.Drawing.Size(0, 13);
            this.lbl_resultado.TabIndex = 12;
            this.lbl_resultado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frm_exe9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 230);
            this.ControlBox = false;
            this.Controls.Add(this.lbl_resultado);
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.btn_calcular);
            this.Controls.Add(this.msk_hr_mn_saida);
            this.Controls.Add(this.lbl_hr_mn_saida);
            this.Controls.Add(this.msk_hr_mn_entrada);
            this.Controls.Add(this.lbl_hr_mn_entrada);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frm_exe9";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estacionamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Button btn_calcular;
        private System.Windows.Forms.MaskedTextBox msk_hr_mn_saida;
        private System.Windows.Forms.Label lbl_hr_mn_saida;
        private System.Windows.Forms.MaskedTextBox msk_hr_mn_entrada;
        private System.Windows.Forms.Label lbl_hr_mn_entrada;
        private System.Windows.Forms.Label lbl_resultado;
    }
}

