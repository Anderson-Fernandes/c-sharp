﻿namespace LISTA2_EXE8_ALCF_ERE
{
    partial class frm_exe8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_sair = new System.Windows.Forms.Button();
            this.lbl_volume = new System.Windows.Forms.Label();
            this.btn_calcular = new System.Windows.Forms.Button();
            this.txt_raio = new System.Windows.Forms.TextBox();
            this.lbl_raio = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(154, 109);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(75, 23);
            this.btn_sair.TabIndex = 9;
            this.btn_sair.Text = "Sair";
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // lbl_volume
            // 
            this.lbl_volume.AutoSize = true;
            this.lbl_volume.Location = new System.Drawing.Point(20, 90);
            this.lbl_volume.Name = "lbl_volume";
            this.lbl_volume.Size = new System.Drawing.Size(0, 13);
            this.lbl_volume.TabIndex = 8;
            // 
            // btn_calcular
            // 
            this.btn_calcular.Location = new System.Drawing.Point(119, 48);
            this.btn_calcular.Name = "btn_calcular";
            this.btn_calcular.Size = new System.Drawing.Size(75, 23);
            this.btn_calcular.TabIndex = 7;
            this.btn_calcular.Text = "Calcular";
            this.btn_calcular.UseVisualStyleBackColor = true;
            this.btn_calcular.Click += new System.EventHandler(this.btn_calcular_Click);
            // 
            // txt_raio
            // 
            this.txt_raio.Location = new System.Drawing.Point(119, 14);
            this.txt_raio.MaxLength = 5;
            this.txt_raio.Name = "txt_raio";
            this.txt_raio.Size = new System.Drawing.Size(100, 20);
            this.txt_raio.TabIndex = 6;
            // 
            // lbl_raio
            // 
            this.lbl_raio.AutoSize = true;
            this.lbl_raio.Location = new System.Drawing.Point(17, 17);
            this.lbl_raio.Name = "lbl_raio";
            this.lbl_raio.Size = new System.Drawing.Size(79, 13);
            this.lbl_raio.TabIndex = 5;
            this.lbl_raio.Text = "Raio da esfera:";
            // 
            // frm_exe8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(254, 154);
            this.ControlBox = false;
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.lbl_volume);
            this.Controls.Add(this.btn_calcular);
            this.Controls.Add(this.txt_raio);
            this.Controls.Add(this.lbl_raio);
            this.Name = "frm_exe8";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Volume da Esfera";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Label lbl_volume;
        private System.Windows.Forms.Button btn_calcular;
        private System.Windows.Forms.TextBox txt_raio;
        private System.Windows.Forms.Label lbl_raio;
    }
}

