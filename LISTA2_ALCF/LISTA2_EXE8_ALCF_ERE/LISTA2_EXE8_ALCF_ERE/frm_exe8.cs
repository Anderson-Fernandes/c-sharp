﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA2_EXE8_ALCF_ERE
{
    public partial class frm_exe8 : Form
    {
        public frm_exe8()
        {
            InitializeComponent();
        }

        private void btn_calcular_Click(object sender, EventArgs e)
        {
            double raioAE, volumeAE;
            raioAE = double.Parse(txt_raio.Text);
            volumeAE = 3.14 * raioAE;
            volumeAE = Math.Pow(volumeAE, 2);
            volumeAE.ToString();
            lbl_volume.Text = "O volume da esfera é " + volumeAE;
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
