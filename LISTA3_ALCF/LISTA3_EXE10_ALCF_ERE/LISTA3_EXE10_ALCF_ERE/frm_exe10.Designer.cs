﻿namespace LISTA3_EXE10_ALCF_ERE
{
    partial class frm_exe10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_sair = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.txt_fkm = new System.Windows.Forms.TextBox();
            this.txt_ikm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_kminicial = new System.Windows.Forms.Label();
            this.txt_vlcombustivel = new System.Windows.Forms.TextBox();
            this.lbl_vlcombustivel = new System.Windows.Forms.Label();
            this.rdb_gasolina = new System.Windows.Forms.RadioButton();
            this.rdb_alcool = new System.Windows.Forms.RadioButton();
            this.lbl_abastecer = new System.Windows.Forms.Label();
            this.lbl_resultado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(26, 200);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(75, 23);
            this.btn_sair.TabIndex = 21;
            this.btn_sair.Text = "Sair";
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(167, 200);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 20;
            this.btn_ok.Text = "Ok";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // txt_fkm
            // 
            this.txt_fkm.Location = new System.Drawing.Point(149, 128);
            this.txt_fkm.MaxLength = 3;
            this.txt_fkm.Name = "txt_fkm";
            this.txt_fkm.Size = new System.Drawing.Size(93, 20);
            this.txt_fkm.TabIndex = 19;
            // 
            // txt_ikm
            // 
            this.txt_ikm.Location = new System.Drawing.Point(149, 96);
            this.txt_ikm.MaxLength = 3;
            this.txt_ikm.Name = "txt_ikm";
            this.txt_ikm.Size = new System.Drawing.Size(93, 20);
            this.txt_ikm.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "KM Final:";
            // 
            // lbl_kminicial
            // 
            this.lbl_kminicial.AutoSize = true;
            this.lbl_kminicial.Location = new System.Drawing.Point(78, 99);
            this.lbl_kminicial.Name = "lbl_kminicial";
            this.lbl_kminicial.Size = new System.Drawing.Size(56, 13);
            this.lbl_kminicial.TabIndex = 16;
            this.lbl_kminicial.Text = "KM Inicial:";
            // 
            // txt_vlcombustivel
            // 
            this.txt_vlcombustivel.Location = new System.Drawing.Point(149, 64);
            this.txt_vlcombustivel.MaxLength = 4;
            this.txt_vlcombustivel.Name = "txt_vlcombustivel";
            this.txt_vlcombustivel.Size = new System.Drawing.Size(93, 20);
            this.txt_vlcombustivel.TabIndex = 15;
            // 
            // lbl_vlcombustivel
            // 
            this.lbl_vlcombustivel.AutoSize = true;
            this.lbl_vlcombustivel.Location = new System.Drawing.Point(23, 67);
            this.lbl_vlcombustivel.Name = "lbl_vlcombustivel";
            this.lbl_vlcombustivel.Size = new System.Drawing.Size(111, 13);
            this.lbl_vlcombustivel.TabIndex = 14;
            this.lbl_vlcombustivel.Text = "Valor do Combustível:";
            // 
            // rdb_gasolina
            // 
            this.rdb_gasolina.AutoSize = true;
            this.rdb_gasolina.Location = new System.Drawing.Point(195, 31);
            this.rdb_gasolina.Name = "rdb_gasolina";
            this.rdb_gasolina.Size = new System.Drawing.Size(66, 17);
            this.rdb_gasolina.TabIndex = 13;
            this.rdb_gasolina.Text = "Gasolina";
            this.rdb_gasolina.UseVisualStyleBackColor = true;
            // 
            // rdb_alcool
            // 
            this.rdb_alcool.AutoSize = true;
            this.rdb_alcool.Location = new System.Drawing.Point(129, 31);
            this.rdb_alcool.Name = "rdb_alcool";
            this.rdb_alcool.Size = new System.Drawing.Size(54, 17);
            this.rdb_alcool.TabIndex = 12;
            this.rdb_alcool.Text = "Álcool";
            this.rdb_alcool.UseVisualStyleBackColor = true;
            // 
            // lbl_abastecer
            // 
            this.lbl_abastecer.AutoSize = true;
            this.lbl_abastecer.Location = new System.Drawing.Point(24, 33);
            this.lbl_abastecer.Name = "lbl_abastecer";
            this.lbl_abastecer.Size = new System.Drawing.Size(81, 13);
            this.lbl_abastecer.TabIndex = 11;
            this.lbl_abastecer.Text = "Abastecer com:";
            // 
            // lbl_resultado
            // 
            this.lbl_resultado.AutoSize = true;
            this.lbl_resultado.Location = new System.Drawing.Point(24, 165);
            this.lbl_resultado.Name = "lbl_resultado";
            this.lbl_resultado.Size = new System.Drawing.Size(0, 13);
            this.lbl_resultado.TabIndex = 22;
            // 
            // frm_exe10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ControlBox = false;
            this.Controls.Add(this.lbl_resultado);
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.txt_fkm);
            this.Controls.Add(this.txt_ikm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_kminicial);
            this.Controls.Add(this.txt_vlcombustivel);
            this.Controls.Add(this.lbl_vlcombustivel);
            this.Controls.Add(this.rdb_gasolina);
            this.Controls.Add(this.rdb_alcool);
            this.Controls.Add(this.lbl_abastecer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frm_exe10";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Combustivel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox txt_fkm;
        private System.Windows.Forms.TextBox txt_ikm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_kminicial;
        private System.Windows.Forms.TextBox txt_vlcombustivel;
        private System.Windows.Forms.Label lbl_vlcombustivel;
        private System.Windows.Forms.RadioButton rdb_gasolina;
        private System.Windows.Forms.RadioButton rdb_alcool;
        private System.Windows.Forms.Label lbl_abastecer;
        private System.Windows.Forms.Label lbl_resultado;
    }
}

