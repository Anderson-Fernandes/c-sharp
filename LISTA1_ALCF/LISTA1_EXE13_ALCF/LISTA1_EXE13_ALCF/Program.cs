﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LISTA1_EXE13_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entre com o valor da aresta");
            double arestA = double.Parse(Console.ReadLine());
            arestA = Math.Pow(arestA, 3);
            Console.WriteLine("Entre com o valor do raio");
            double raioA = double.Parse(Console.ReadLine());
            raioA = Math.PI * Math.Pow(raioA, 3) * 4.0 / 3;
            double resul = arestA - raioA;
            Console.WriteLine("A Aresta do cubo é: {0}", arestA);
            Console.WriteLine("O raio da esfera é: {0}", raioA);
            Console.WriteLine("O espaço vazio do cubo é: {0}", resul);
            Console.ReadKey();
        }
    }
}