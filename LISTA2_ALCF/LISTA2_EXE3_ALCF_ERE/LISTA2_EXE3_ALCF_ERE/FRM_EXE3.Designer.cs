﻿namespace LISTA2_EXE3_ALCF_ERE
{
    partial class FRM_EXE3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_valor = new System.Windows.Forms.Label();
            this.lbl_prestacao = new System.Windows.Forms.Label();
            this.txt_valor = new System.Windows.Forms.TextBox();
            this.txt_parce = new System.Windows.Forms.TextBox();
            this.btn_calcular = new System.Windows.Forms.Button();
            this.lbl_result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_valor
            // 
            this.lbl_valor.AutoSize = true;
            this.lbl_valor.Location = new System.Drawing.Point(13, 13);
            this.lbl_valor.Name = "lbl_valor";
            this.lbl_valor.Size = new System.Drawing.Size(128, 13);
            this.lbl_valor.TabIndex = 0;
            this.lbl_valor.Text = "Digite o valor da compra: ";
            // 
            // lbl_prestacao
            // 
            this.lbl_prestacao.AutoSize = true;
            this.lbl_prestacao.Location = new System.Drawing.Point(13, 45);
            this.lbl_prestacao.Name = "lbl_prestacao";
            this.lbl_prestacao.Size = new System.Drawing.Size(157, 13);
            this.lbl_prestacao.TabIndex = 1;
            this.lbl_prestacao.Text = "Digite o número de prestações: ";
            // 
            // txt_valor
            // 
            this.txt_valor.Location = new System.Drawing.Point(147, 10);
            this.txt_valor.Name = "txt_valor";
            this.txt_valor.Size = new System.Drawing.Size(98, 20);
            this.txt_valor.TabIndex = 2;
            // 
            // txt_parce
            // 
            this.txt_parce.Location = new System.Drawing.Point(176, 42);
            this.txt_parce.Name = "txt_parce";
            this.txt_parce.Size = new System.Drawing.Size(82, 20);
            this.txt_parce.TabIndex = 3;
            // 
            // btn_calcular
            // 
            this.btn_calcular.Location = new System.Drawing.Point(109, 136);
            this.btn_calcular.Name = "btn_calcular";
            this.btn_calcular.Size = new System.Drawing.Size(70, 28);
            this.btn_calcular.TabIndex = 4;
            this.btn_calcular.Text = "Calcular";
            this.btn_calcular.UseVisualStyleBackColor = true;
            this.btn_calcular.Click += new System.EventHandler(this.btn_calcular_Click);
            // 
            // lbl_result
            // 
            this.lbl_result.AutoSize = true;
            this.lbl_result.ForeColor = System.Drawing.Color.Red;
            this.lbl_result.Location = new System.Drawing.Point(13, 196);
            this.lbl_result.Name = "lbl_result";
            this.lbl_result.Size = new System.Drawing.Size(0, 13);
            this.lbl_result.TabIndex = 5;
            // 
            // FRM_EXE3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lbl_result);
            this.Controls.Add(this.btn_calcular);
            this.Controls.Add(this.txt_parce);
            this.Controls.Add(this.txt_valor);
            this.Controls.Add(this.lbl_prestacao);
            this.Controls.Add(this.lbl_valor);
            this.Name = "FRM_EXE3";
            this.Text = "Prestações";
            this.Load += new System.EventHandler(this.FRM_EXE3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_valor;
        private System.Windows.Forms.Label lbl_prestacao;
        private System.Windows.Forms.TextBox txt_valor;
        private System.Windows.Forms.TextBox txt_parce;
        private System.Windows.Forms.Button btn_calcular;
        private System.Windows.Forms.Label lbl_result;
    }
}

