﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LISTA4_EXE1_ALCF_ERE
{
    public partial class Frm_exe1 : Form
    {
        public Frm_exe1()
        {
            InitializeComponent();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            txt_cliente.Text = null;
            txt_valor.Text = null;
            rdb_sim.Checked = false;
            rdb_nao.Checked = false;
            rdb_avista.Checked = false;
            rdb_tD.Checked = false;
            rdb_cD.Checked = false;
            rdb_joaquim.Checked = false;
            rdb_ernesto.Checked = false;
            rdb_maria.Checked = false;
            rdb_carlos.Checked = false;
            rdb_cD.Enabled = true;
            rdb_tD.Enabled = true;
        }

        private void txt_valor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || Char.IsSymbol(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void rdb_sim_CheckedChanged(object sender, EventArgs e)
        {
            rdb_avista.Checked = true;
            rdb_cD.Enabled = false;
            rdb_tD.Enabled = false;
        }

        private void rdb_nao_CheckedChanged(object sender, EventArgs e)
        {
            checking();
         }

        private void txt_cliente_KeyPress(object sender, KeyPressEventArgs e)
        {
           if(Char.IsNumber(e.KeyChar) || Char.IsSymbol(e.KeyChar))
           {
               e.Handled = true;
           }
        }

        private void btn_confirmar_Click(object sender, EventArgs e)
        {
            if (txt_valor.TextLength < 1 || txt_cliente.TextLength < 1 || condicaoP())
            {
                MessageBox.Show("Preencha todos os campos!", "ATENÇÃO", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Cliente: " + txt_cliente.Text + "\nValor da Compra: " + txt_valor.Text + "\nParcelas: " + parcelas(), "Dados da Compra", MessageBoxButtons.OK);
            }
        }

        private void txt_valor_Leave(object sender, EventArgs e)
        {
            checking();
        }

        public void checking()
        {
           try
            {
                if (Double.Parse(txt_valor.Text) > 200 && rdb_sim.Checked == false)
                {
                    rdb_cD.Enabled = true;
                }
                else if (Double.Parse(txt_valor.Text) < 201 && rdb_sim.Checked == false)
                {
                    rdb_cD.Checked = false;
                    rdb_cD.Enabled = false;
                    rdb_tD.Enabled = true;
                }
            }
            catch(FormatException)
            {
                rdb_cD.Enabled = true;
                rdb_tD.Enabled = true;
            }
        }

        public String parcelas()
        {
             String parcelas;
            if (rdb_avista.Checked == true)
            {
                return parcelas="A Vista";
            }
            else if (rdb_tD.Checked == true)
            {
                return parcelas = "30x";
            }
            else if (rdb_cD.Checked == true)
            {
                return parcelas = "60x";
            }
            return parcelas = "";
        }

        public Boolean condicaoP()
        {
            if (rdb_avista.Checked == false && rdb_tD.Checked == false && rdb_cD.Checked == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
