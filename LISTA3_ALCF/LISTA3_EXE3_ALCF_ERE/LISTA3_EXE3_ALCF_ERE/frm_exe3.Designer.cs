﻿namespace LISTA3_EXE3_ALCF_ERE
{
    partial class frm_exe3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_a = new System.Windows.Forms.Label();
            this.txt_a = new System.Windows.Forms.TextBox();
            this.lbl_b = new System.Windows.Forms.Label();
            this.txt_b = new System.Windows.Forms.TextBox();
            this.lbl_c = new System.Windows.Forms.Label();
            this.txt_c = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_tipo = new System.Windows.Forms.Button();
            this.lbl_tipo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_a
            // 
            this.lbl_a.AutoSize = true;
            this.lbl_a.Location = new System.Drawing.Point(37, 81);
            this.lbl_a.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbl_a.Name = "lbl_a";
            this.lbl_a.Size = new System.Drawing.Size(77, 18);
            this.lbl_a.TabIndex = 0;
            this.lbl_a.Text = "Lado A: ";
            // 
            // txt_a
            // 
            this.txt_a.Location = new System.Drawing.Point(124, 78);
            this.txt_a.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_a.Name = "txt_a";
            this.txt_a.Size = new System.Drawing.Size(164, 27);
            this.txt_a.TabIndex = 1;
            // 
            // lbl_b
            // 
            this.lbl_b.AutoSize = true;
            this.lbl_b.Location = new System.Drawing.Point(37, 119);
            this.lbl_b.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbl_b.Name = "lbl_b";
            this.lbl_b.Size = new System.Drawing.Size(77, 18);
            this.lbl_b.TabIndex = 2;
            this.lbl_b.Text = "Lado B: ";
            // 
            // txt_b
            // 
            this.txt_b.Location = new System.Drawing.Point(124, 116);
            this.txt_b.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_b.Name = "txt_b";
            this.txt_b.Size = new System.Drawing.Size(164, 27);
            this.txt_b.TabIndex = 3;
            // 
            // lbl_c
            // 
            this.lbl_c.AutoSize = true;
            this.lbl_c.Location = new System.Drawing.Point(37, 154);
            this.lbl_c.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbl_c.Name = "lbl_c";
            this.lbl_c.Size = new System.Drawing.Size(77, 18);
            this.lbl_c.TabIndex = 4;
            this.lbl_c.Text = "Lado C: ";
            // 
            // txt_c
            // 
            this.txt_c.Location = new System.Drawing.Point(124, 151);
            this.txt_c.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txt_c.Name = "txt_c";
            this.txt_c.Size = new System.Drawing.Size(164, 27);
            this.txt_c.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(100, 37);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Tipo de Triangulo";
            // 
            // btn_tipo
            // 
            this.btn_tipo.Location = new System.Drawing.Point(138, 197);
            this.btn_tipo.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btn_tipo.Name = "btn_tipo";
            this.btn_tipo.Size = new System.Drawing.Size(125, 32);
            this.btn_tipo.TabIndex = 7;
            this.btn_tipo.Text = "Verificar";
            this.btn_tipo.UseVisualStyleBackColor = true;
            this.btn_tipo.Click += new System.EventHandler(this.btn_tipo_Click);
            // 
            // lbl_tipo
            // 
            this.lbl_tipo.AutoSize = true;
            this.lbl_tipo.Location = new System.Drawing.Point(55, 247);
            this.lbl_tipo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbl_tipo.Name = "lbl_tipo";
            this.lbl_tipo.Size = new System.Drawing.Size(0, 18);
            this.lbl_tipo.TabIndex = 8;
            // 
            // frm_exe3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 294);
            this.Controls.Add(this.lbl_tipo);
            this.Controls.Add(this.btn_tipo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_c);
            this.Controls.Add(this.lbl_c);
            this.Controls.Add(this.txt_b);
            this.Controls.Add(this.lbl_b);
            this.Controls.Add(this.txt_a);
            this.Controls.Add(this.lbl_a);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "frm_exe3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analisar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_a;
        private System.Windows.Forms.TextBox txt_a;
        private System.Windows.Forms.Label lbl_b;
        private System.Windows.Forms.TextBox txt_b;
        private System.Windows.Forms.Label lbl_c;
        private System.Windows.Forms.TextBox txt_c;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_tipo;
        private System.Windows.Forms.Button btn_tipo;
    }
}

