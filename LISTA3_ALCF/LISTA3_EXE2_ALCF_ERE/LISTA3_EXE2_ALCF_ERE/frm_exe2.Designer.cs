﻿namespace LISTA3_EXE2_ALCF_ERE
{
    partial class frm_exe2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.lbl_caixa = new System.Windows.Forms.Label();
            this.bnt_enviar = new System.Windows.Forms.Button();
            this.lbl_produto = new System.Windows.Forms.Label();
            this.lbl_vlProduto = new System.Windows.Forms.Label();
            this.lbl_qtProduto = new System.Windows.Forms.Label();
            this.lbl_qtCaixa = new System.Windows.Forms.Label();
            this.txt_vlProduto = new System.Windows.Forms.TextBox();
            this.txt_qtProduto = new System.Windows.Forms.TextBox();
            this.txt_qtCaixa = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_limpar);
            this.panel1.Controls.Add(this.lbl_caixa);
            this.panel1.Controls.Add(this.bnt_enviar);
            this.panel1.Controls.Add(this.lbl_produto);
            this.panel1.Controls.Add(this.lbl_vlProduto);
            this.panel1.Controls.Add(this.lbl_qtProduto);
            this.panel1.Controls.Add(this.lbl_qtCaixa);
            this.panel1.Controls.Add(this.txt_vlProduto);
            this.panel1.Controls.Add(this.txt_qtProduto);
            this.panel1.Controls.Add(this.txt_qtCaixa);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 349);
            this.panel1.TabIndex = 0;
            // 
            // btn_limpar
            // 
            this.btn_limpar.Location = new System.Drawing.Point(108, 293);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 30);
            this.btn_limpar.TabIndex = 9;
            this.btn_limpar.Text = "Limpar";
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // lbl_caixa
            // 
            this.lbl_caixa.AutoSize = true;
            this.lbl_caixa.Font = new System.Drawing.Font("Verdana", 25F);
            this.lbl_caixa.Location = new System.Drawing.Point(134, 17);
            this.lbl_caixa.Name = "lbl_caixa";
            this.lbl_caixa.Size = new System.Drawing.Size(111, 41);
            this.lbl_caixa.TabIndex = 8;
            this.lbl_caixa.Text = "Caixa";
            this.lbl_caixa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bnt_enviar
            // 
            this.bnt_enviar.Location = new System.Drawing.Point(205, 293);
            this.bnt_enviar.Name = "bnt_enviar";
            this.bnt_enviar.Size = new System.Drawing.Size(87, 30);
            this.bnt_enviar.TabIndex = 7;
            this.bnt_enviar.Text = "Calcular";
            this.bnt_enviar.UseVisualStyleBackColor = true;
            this.bnt_enviar.Click += new System.EventHandler(this.bnt_enviar_Click);
            // 
            // lbl_produto
            // 
            this.lbl_produto.AutoSize = true;
            this.lbl_produto.Font = new System.Drawing.Font("Verdana", 25F);
            this.lbl_produto.Location = new System.Drawing.Point(122, 124);
            this.lbl_produto.Name = "lbl_produto";
            this.lbl_produto.Size = new System.Drawing.Size(152, 41);
            this.lbl_produto.TabIndex = 6;
            this.lbl_produto.Text = "Produto";
            this.lbl_produto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_vlProduto
            // 
            this.lbl_vlProduto.AutoSize = true;
            this.lbl_vlProduto.Location = new System.Drawing.Point(65, 241);
            this.lbl_vlProduto.Name = "lbl_vlProduto";
            this.lbl_vlProduto.Size = new System.Drawing.Size(65, 18);
            this.lbl_vlProduto.TabIndex = 5;
            this.lbl_vlProduto.Text = "Preço: ";
            // 
            // lbl_qtProduto
            // 
            this.lbl_qtProduto.AutoSize = true;
            this.lbl_qtProduto.Location = new System.Drawing.Point(26, 185);
            this.lbl_qtProduto.Name = "lbl_qtProduto";
            this.lbl_qtProduto.Size = new System.Drawing.Size(110, 18);
            this.lbl_qtProduto.TabIndex = 4;
            this.lbl_qtProduto.Text = "Quantidade:";
            // 
            // lbl_qtCaixa
            // 
            this.lbl_qtCaixa.AutoSize = true;
            this.lbl_qtCaixa.Location = new System.Drawing.Point(26, 85);
            this.lbl_qtCaixa.Name = "lbl_qtCaixa";
            this.lbl_qtCaixa.Size = new System.Drawing.Size(116, 18);
            this.lbl_qtCaixa.TabIndex = 3;
            this.lbl_qtCaixa.Text = "Quantidade: ";
            // 
            // txt_vlProduto
            // 
            this.txt_vlProduto.Location = new System.Drawing.Point(141, 238);
            this.txt_vlProduto.Name = "txt_vlProduto";
            this.txt_vlProduto.Size = new System.Drawing.Size(151, 27);
            this.txt_vlProduto.TabIndex = 2;
            // 
            // txt_qtProduto
            // 
            this.txt_qtProduto.Location = new System.Drawing.Point(141, 182);
            this.txt_qtProduto.Name = "txt_qtProduto";
            this.txt_qtProduto.Size = new System.Drawing.Size(151, 27);
            this.txt_qtProduto.TabIndex = 1;
            // 
            // txt_qtCaixa
            // 
            this.txt_qtCaixa.Location = new System.Drawing.Point(141, 82);
            this.txt_qtCaixa.Name = "txt_qtCaixa";
            this.txt_qtCaixa.Size = new System.Drawing.Size(151, 27);
            this.txt_qtCaixa.TabIndex = 0;
            // 
            // frm_exe2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(364, 373);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.MaximizeBox = false;
            this.Name = "frm_exe2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Caixa";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_vlProduto;
        private System.Windows.Forms.Label lbl_qtProduto;
        private System.Windows.Forms.Label lbl_qtCaixa;
        private System.Windows.Forms.TextBox txt_vlProduto;
        private System.Windows.Forms.TextBox txt_qtProduto;
        private System.Windows.Forms.TextBox txt_qtCaixa;
        private System.Windows.Forms.Button bnt_enviar;
        private System.Windows.Forms.Label lbl_produto;
        private System.Windows.Forms.Label lbl_caixa;
        private System.Windows.Forms.Button btn_limpar;
    }
}

