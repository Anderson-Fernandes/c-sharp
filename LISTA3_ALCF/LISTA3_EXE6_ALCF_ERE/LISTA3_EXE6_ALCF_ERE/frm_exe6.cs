﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE6_ALCF_ERE
{
    public partial class frm_exe6 : Form
    {
        public frm_exe6()
        {
            InitializeComponent();
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (txt_temperatura.TextLength < 1)
            {
                MessageBox.Show("Preencha todos os campos!", "ATENÇÃO!", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    int tempAE;
                    tempAE = int.Parse(txt_temperatura.Text);

                    if (tempAE < 100)
                    {
                        mensage("A temperatura está muito baixa");
                    }
                    else
                    {
                        if (tempAE >= 100 && tempAE <= 200)
                        {
                            mensage("A temperatura está baixa");
                        }
                        else
                        {
                            if (tempAE > 200 && tempAE < 500)
                            {
                                mensage("A temperatura está normal");
                            }
                            else
                            {
                                if (tempAE > 500)
                                {
                                    mensage("A temperatura está muito alta");
                                }
                            }
                        }
                    }
                }
                catch(FormatException)
                {
                    MessageBox.Show("Por Favor, Digite apenas numeros","ATENÇÃO!",MessageBoxButtons.OK);
                }
                catch(OverflowException)
                {
                    MessageBox.Show("Por Favor, Digite um numero menor","ATENÇÃO!",MessageBoxButtons.OK);
                }
            }
        }

        public void mensage(String label)
        {
            MessageBox.Show(label, "Temperatura!", MessageBoxButtons.OK);
        }
    }
}
