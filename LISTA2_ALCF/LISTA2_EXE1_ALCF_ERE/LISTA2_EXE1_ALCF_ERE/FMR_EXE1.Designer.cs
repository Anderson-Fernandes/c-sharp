﻿namespace LISTA2_EXE1_ALCF_ERE
{
    partial class FMR_EXE1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ndA = new System.Windows.Forms.Label();
            this.txt_valorA = new System.Windows.Forms.TextBox();
            this.lbl_ndB = new System.Windows.Forms.Label();
            this.txt_valorB = new System.Windows.Forms.TextBox();
            this.btn_misturar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_ndA
            // 
            this.lbl_ndA.AutoSize = true;
            this.lbl_ndA.Location = new System.Drawing.Point(12, 9);
            this.lbl_ndA.Name = "lbl_ndA";
            this.lbl_ndA.Size = new System.Drawing.Size(85, 13);
            this.lbl_ndA.TabIndex = 0;
            this.lbl_ndA.Text = "Digite o valor A: ";
            this.lbl_ndA.Click += new System.EventHandler(this.label1_Click);
            // 
            // txt_valorA
            // 
            this.txt_valorA.Location = new System.Drawing.Point(103, 6);
            this.txt_valorA.Name = "txt_valorA";
            this.txt_valorA.Size = new System.Drawing.Size(105, 20);
            this.txt_valorA.TabIndex = 1;
            // 
            // lbl_ndB
            // 
            this.lbl_ndB.AutoSize = true;
            this.lbl_ndB.Location = new System.Drawing.Point(12, 44);
            this.lbl_ndB.Name = "lbl_ndB";
            this.lbl_ndB.Size = new System.Drawing.Size(82, 13);
            this.lbl_ndB.TabIndex = 2;
            this.lbl_ndB.Text = "Digite o valor B:";
            this.lbl_ndB.Click += new System.EventHandler(this.lbl_ndB_Click);
            // 
            // txt_valorB
            // 
            this.txt_valorB.Location = new System.Drawing.Point(103, 41);
            this.txt_valorB.Name = "txt_valorB";
            this.txt_valorB.Size = new System.Drawing.Size(105, 20);
            this.txt_valorB.TabIndex = 3;
            // 
            // btn_misturar
            // 
            this.btn_misturar.Location = new System.Drawing.Point(103, 195);
            this.btn_misturar.Name = "btn_misturar";
            this.btn_misturar.Size = new System.Drawing.Size(105, 34);
            this.btn_misturar.TabIndex = 6;
            this.btn_misturar.Text = "Misturar";
            this.btn_misturar.UseVisualStyleBackColor = true;
            this.btn_misturar.Click += new System.EventHandler(this.btn_misturar_Click);
            // 
            // FMR_EXE1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn_misturar);
            this.Controls.Add(this.txt_valorB);
            this.Controls.Add(this.lbl_ndB);
            this.Controls.Add(this.txt_valorA);
            this.Controls.Add(this.lbl_ndA);
            this.Name = "FMR_EXE1";
            this.ShowIcon = false;
            this.Text = "Mistura";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ndA;
        private System.Windows.Forms.TextBox txt_valorA;
        private System.Windows.Forms.Label lbl_ndB;
        private System.Windows.Forms.TextBox txt_valorB;
        private System.Windows.Forms.Button btn_misturar;
    }
}

