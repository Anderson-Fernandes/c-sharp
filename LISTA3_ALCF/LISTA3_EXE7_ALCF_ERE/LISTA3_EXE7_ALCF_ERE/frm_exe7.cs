﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA3_EXE7_ALCF_ERE
{
    public partial class frm_exe7 : Form
    {
        public frm_exe7()
        {
            InitializeComponent();
        }

        private void frm_exe7_Load(object sender, EventArgs e)
        {

        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_calcular_Click(object sender, EventArgs e)
        {
            if (txt_ht.TextLength < 1 || txt_vh.TextLength < 1)
            {
                MessageBox.Show("Preencha todos os campos", "ATENÇÃO!", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    int htAE;
                    double vhAE, sbAE, inssAE, irAE, aAE, vdAE, slAE;
                    htAE = int.Parse(txt_ht.Text);
                    vhAE = double.Parse(txt_vh.Text);
                    sbAE = htAE * vhAE;
                    inssAE = sbAE * 0.11;
                    if (sbAE <= 1257.12)
                    {
                        aAE = 0;
                        vdAE = 0;
                        irAE = aAE * (sbAE - inssAE) - vdAE;
                        slAE = (sbAE - inssAE) - irAE;
                        men("O salário líquido do empregado é: " + slAE.ToString("C"));
                    }
                    else
                    {
                        if (sbAE >= 1257.13 && sbAE <= 2512.08)
                        {
                            aAE = 0.15;
                            vdAE = 188.57;
                            irAE = aAE * (sbAE - inssAE) - vdAE;
                            slAE = (sbAE - inssAE) - irAE;
                            men("O salário líquido do empregado é: " + slAE.ToString("C"));
                        }
                        else
                        {
                            if (sbAE > 2512.08)
                            {
                                aAE = 2.75;
                                vdAE = 502.58;
                                irAE = aAE * (sbAE - inssAE) - vdAE;
                                slAE = (sbAE - inssAE) - irAE;
                                slAE = slAE * (-1);
                                men("O salário líquido do empregado é: " + slAE.ToString("C"));
                            }
                        }
                    }
                }
                catch(FormatException)
                {
                    MessageBox.Show("Por favor, digite apenas numeros","ATENÇÃO",MessageBoxButtons.OK);
                }
                catch(OverflowException)
                {
                    MessageBox.Show("Digite um numero menor","ATENÇÃO!",MessageBoxButtons.OK);
                }
            }
        }      
       public void men(String labelAE)
       {
           MessageBox.Show(labelAE,"Salario Liquido",MessageBoxButtons.OK);   
       }
    }
}
