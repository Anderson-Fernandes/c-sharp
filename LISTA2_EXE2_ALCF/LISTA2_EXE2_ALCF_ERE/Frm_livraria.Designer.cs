﻿namespace LISTA2_EXE2_ALCF_ERE
{
    partial class Frm_livraria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_livro = new System.Windows.Forms.TextBox();
            this.txt_autor = new System.Windows.Forms.TextBox();
            this.txt_monitario = new System.Windows.Forms.TextBox();
            this.cmb_categoria = new System.Windows.Forms.ComboBox();
            this.lbl_livro = new System.Windows.Forms.Label();
            this.lbl_autor = new System.Windows.Forms.Label();
            this.lbl_categoria = new System.Windows.Forms.Label();
            this.chck_ebook = new System.Windows.Forms.CheckBox();
            this.lbl_valor = new System.Windows.Forms.Label();
            this.lbl_pagamento = new System.Windows.Forms.Label();
            this.cmb_pagamento = new System.Windows.Forms.ComboBox();
            this.btn_concluir = new System.Windows.Forms.Button();
            this.lbl_avista = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_livro
            // 
            this.txt_livro.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txt_livro.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txt_livro.Location = new System.Drawing.Point(153, 81);
            this.txt_livro.Margin = new System.Windows.Forms.Padding(5);
            this.txt_livro.Name = "txt_livro";
            this.txt_livro.Size = new System.Drawing.Size(180, 29);
            this.txt_livro.TabIndex = 0;
            this.txt_livro.TextChanged += new System.EventHandler(this.txt_livro_TextChanged);
            // 
            // txt_autor
            // 
            this.txt_autor.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txt_autor.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txt_autor.Location = new System.Drawing.Point(153, 137);
            this.txt_autor.Margin = new System.Windows.Forms.Padding(5);
            this.txt_autor.Name = "txt_autor";
            this.txt_autor.Size = new System.Drawing.Size(180, 29);
            this.txt_autor.TabIndex = 1;
            this.txt_autor.TextChanged += new System.EventHandler(this.txt_autor_TextChanged);
            // 
            // txt_monitario
            // 
            this.txt_monitario.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txt_monitario.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txt_monitario.Location = new System.Drawing.Point(153, 235);
            this.txt_monitario.Margin = new System.Windows.Forms.Padding(5);
            this.txt_monitario.Name = "txt_monitario";
            this.txt_monitario.Size = new System.Drawing.Size(180, 29);
            this.txt_monitario.TabIndex = 2;
            this.txt_monitario.TextChanged += new System.EventHandler(this.txt_monitario_TextChanged);
            // 
            // cmb_categoria
            // 
            this.cmb_categoria.FormattingEnabled = true;
            this.cmb_categoria.Items.AddRange(new object[] {
            "Romance",
            "Ação",
            "Suspence",
            "Infatil"});
            this.cmb_categoria.Location = new System.Drawing.Point(153, 197);
            this.cmb_categoria.Margin = new System.Windows.Forms.Padding(5);
            this.cmb_categoria.Name = "cmb_categoria";
            this.cmb_categoria.Size = new System.Drawing.Size(180, 28);
            this.cmb_categoria.TabIndex = 3;
            this.cmb_categoria.SelectedIndexChanged += new System.EventHandler(this.cmb_categoria_SelectedIndexChanged);
            // 
            // lbl_livro
            // 
            this.lbl_livro.AutoSize = true;
            this.lbl_livro.Font = new System.Drawing.Font("Verdana", 13F);
            this.lbl_livro.Location = new System.Drawing.Point(47, 81);
            this.lbl_livro.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbl_livro.Name = "lbl_livro";
            this.lbl_livro.Size = new System.Drawing.Size(55, 22);
            this.lbl_livro.TabIndex = 4;
            this.lbl_livro.Text = "Livro";
            // 
            // lbl_autor
            // 
            this.lbl_autor.AutoSize = true;
            this.lbl_autor.Font = new System.Drawing.Font("Verdana", 13F);
            this.lbl_autor.Location = new System.Drawing.Point(47, 144);
            this.lbl_autor.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbl_autor.Name = "lbl_autor";
            this.lbl_autor.Size = new System.Drawing.Size(59, 22);
            this.lbl_autor.TabIndex = 5;
            this.lbl_autor.Text = "Autor";
            // 
            // lbl_categoria
            // 
            this.lbl_categoria.AutoSize = true;
            this.lbl_categoria.Location = new System.Drawing.Point(47, 197);
            this.lbl_categoria.Name = "lbl_categoria";
            this.lbl_categoria.Size = new System.Drawing.Size(98, 22);
            this.lbl_categoria.TabIndex = 6;
            this.lbl_categoria.Text = "Categoria";
            // 
            // chck_ebook
            // 
            this.chck_ebook.AutoSize = true;
            this.chck_ebook.Location = new System.Drawing.Point(153, 288);
            this.chck_ebook.Name = "chck_ebook";
            this.chck_ebook.Size = new System.Drawing.Size(174, 26);
            this.chck_ebook.TabIndex = 7;
            this.chck_ebook.Text = "Possui no ebook";
            this.chck_ebook.UseVisualStyleBackColor = true;
            // 
            // lbl_valor
            // 
            this.lbl_valor.AutoSize = true;
            this.lbl_valor.Location = new System.Drawing.Point(49, 242);
            this.lbl_valor.Name = "lbl_valor";
            this.lbl_valor.Size = new System.Drawing.Size(57, 22);
            this.lbl_valor.TabIndex = 8;
            this.lbl_valor.Text = "Valor";
            // 
            // lbl_pagamento
            // 
            this.lbl_pagamento.AutoSize = true;
            this.lbl_pagamento.Location = new System.Drawing.Point(49, 332);
            this.lbl_pagamento.Name = "lbl_pagamento";
            this.lbl_pagamento.Size = new System.Drawing.Size(184, 22);
            this.lbl_pagamento.TabIndex = 10;
            this.lbl_pagamento.Text = "Tipo de Pagamento";
            // 
            // cmb_pagamento
            // 
            this.cmb_pagamento.FormattingEnabled = true;
            this.cmb_pagamento.Items.AddRange(new object[] {
            "Avista",
            "2x",
            "4x",
            "6x",
            "8x",
            "12x"});
            this.cmb_pagamento.Location = new System.Drawing.Point(241, 326);
            this.cmb_pagamento.Margin = new System.Windows.Forms.Padding(5);
            this.cmb_pagamento.Name = "cmb_pagamento";
            this.cmb_pagamento.Size = new System.Drawing.Size(92, 28);
            this.cmb_pagamento.TabIndex = 11;
            this.cmb_pagamento.SelectedIndexChanged += new System.EventHandler(this.cmb_pagamento_SelectedIndexChanged);
            // 
            // btn_concluir
            // 
            this.btn_concluir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_concluir.Location = new System.Drawing.Point(181, 388);
            this.btn_concluir.Name = "btn_concluir";
            this.btn_concluir.Size = new System.Drawing.Size(111, 30);
            this.btn_concluir.TabIndex = 12;
            this.btn_concluir.Text = "Concluir";
            this.btn_concluir.UseVisualStyleBackColor = true;
            this.btn_concluir.Click += new System.EventHandler(this.btn_concluir_Click_1);
            // 
            // lbl_avista
            // 
            this.lbl_avista.AutoSize = true;
            this.lbl_avista.Location = new System.Drawing.Point(51, 448);
            this.lbl_avista.Name = "lbl_avista";
            this.lbl_avista.Size = new System.Drawing.Size(16, 22);
            this.lbl_avista.TabIndex = 13;
            this.lbl_avista.Text = " ";
            this.lbl_avista.Click += new System.EventHandler(this.lbl_avista_Click);
            // 
            // Frm_livraria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(412, 589);
            this.ControlBox = false;
            this.Controls.Add(this.lbl_avista);
            this.Controls.Add(this.btn_concluir);
            this.Controls.Add(this.cmb_pagamento);
            this.Controls.Add(this.lbl_pagamento);
            this.Controls.Add(this.lbl_valor);
            this.Controls.Add(this.chck_ebook);
            this.Controls.Add(this.lbl_categoria);
            this.Controls.Add(this.lbl_autor);
            this.Controls.Add(this.lbl_livro);
            this.Controls.Add(this.cmb_categoria);
            this.Controls.Add(this.txt_monitario);
            this.Controls.Add(this.txt_autor);
            this.Controls.Add(this.txt_livro);
            this.Font = new System.Drawing.Font("Verdana", 13F);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_livraria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Livraria Ipanema";
            this.Load += new System.EventHandler(this.Frm_livraria_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_livro;
        private System.Windows.Forms.TextBox txt_autor;
        private System.Windows.Forms.TextBox txt_monitario;
        private System.Windows.Forms.ComboBox cmb_categoria;
        private System.Windows.Forms.Label lbl_livro;
        private System.Windows.Forms.Label lbl_autor;
        private System.Windows.Forms.Label lbl_categoria;
        private System.Windows.Forms.CheckBox chck_ebook;
        private System.Windows.Forms.Label lbl_valor;
        private System.Windows.Forms.Label lbl_pagamento;
        private System.Windows.Forms.ComboBox cmb_pagamento;
        private System.Windows.Forms.Button btn_concluir;
        private System.Windows.Forms.Label lbl_avista;
    }
}

