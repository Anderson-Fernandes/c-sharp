﻿namespace LISTA2_EXE11_ALCF_ERE
{
    partial class FRM_EXE11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_capacidade = new System.Windows.Forms.Label();
            this.rdb_gasolina = new System.Windows.Forms.RadioButton();
            this.rdb_alcool = new System.Windows.Forms.RadioButton();
            this.btn_calc = new System.Windows.Forms.Button();
            this.txt_capacidade = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_capacidade
            // 
            this.lbl_capacidade.AutoSize = true;
            this.lbl_capacidade.Location = new System.Drawing.Point(12, 23);
            this.lbl_capacidade.Name = "lbl_capacidade";
            this.lbl_capacidade.Size = new System.Drawing.Size(118, 13);
            this.lbl_capacidade.TabIndex = 0;
            this.lbl_capacidade.Text = "Capacidade do tanque:";
            // 
            // rdb_gasolina
            // 
            this.rdb_gasolina.AutoSize = true;
            this.rdb_gasolina.Location = new System.Drawing.Point(15, 62);
            this.rdb_gasolina.Name = "rdb_gasolina";
            this.rdb_gasolina.Size = new System.Drawing.Size(66, 17);
            this.rdb_gasolina.TabIndex = 1;
            this.rdb_gasolina.TabStop = true;
            this.rdb_gasolina.Text = "Gasolina";
            this.rdb_gasolina.UseVisualStyleBackColor = true;
            this.rdb_gasolina.CheckedChanged += new System.EventHandler(this.rdb_gasolina_CheckedChanged);
            // 
            // rdb_alcool
            // 
            this.rdb_alcool.AutoSize = true;
            this.rdb_alcool.Location = new System.Drawing.Point(15, 85);
            this.rdb_alcool.Name = "rdb_alcool";
            this.rdb_alcool.Size = new System.Drawing.Size(54, 17);
            this.rdb_alcool.TabIndex = 2;
            this.rdb_alcool.TabStop = true;
            this.rdb_alcool.Text = "Alcool";
            this.rdb_alcool.UseVisualStyleBackColor = true;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(101, 150);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(88, 23);
            this.btn_calc.TabIndex = 3;
            this.btn_calc.Text = "Calcular";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // txt_capacidade
            // 
            this.txt_capacidade.Location = new System.Drawing.Point(157, 20);
            this.txt_capacidade.Name = "txt_capacidade";
            this.txt_capacidade.Size = new System.Drawing.Size(100, 20);
            this.txt_capacidade.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txt_capacidade);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.rdb_alcool);
            this.Controls.Add(this.rdb_gasolina);
            this.Controls.Add(this.lbl_capacidade);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Valor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_capacidade;
        private System.Windows.Forms.RadioButton rdb_gasolina;
        private System.Windows.Forms.RadioButton rdb_alcool;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.TextBox txt_capacidade;
    }
}

