﻿namespace ExemploAula06
{
    partial class Exemplo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkMilhar = new System.Windows.Forms.CheckBox();
            this.chkCentena = new System.Windows.Forms.CheckBox();
            this.chkDezena = new System.Windows.Forms.CheckBox();
            this.chkUnidade = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.grpMilhar = new System.Windows.Forms.GroupBox();
            this.rdbMilhar7 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar8 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar5 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar2 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar3 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar9 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar4 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar6 = new System.Windows.Forms.RadioButton();
            this.rdbMilhar1 = new System.Windows.Forms.RadioButton();
            this.grpCentena = new System.Windows.Forms.GroupBox();
            this.rdbCentena7 = new System.Windows.Forms.RadioButton();
            this.rdbCentena8 = new System.Windows.Forms.RadioButton();
            this.rdbCentena5 = new System.Windows.Forms.RadioButton();
            this.rdbCentena2 = new System.Windows.Forms.RadioButton();
            this.rdbCentena3 = new System.Windows.Forms.RadioButton();
            this.rdbCentena9 = new System.Windows.Forms.RadioButton();
            this.rdbCentena4 = new System.Windows.Forms.RadioButton();
            this.rdbCentena6 = new System.Windows.Forms.RadioButton();
            this.rdbCentena1 = new System.Windows.Forms.RadioButton();
            this.grpDezena = new System.Windows.Forms.GroupBox();
            this.rdbDezena7 = new System.Windows.Forms.RadioButton();
            this.rdbDezena8 = new System.Windows.Forms.RadioButton();
            this.rdbDezena5 = new System.Windows.Forms.RadioButton();
            this.rdbDezena2 = new System.Windows.Forms.RadioButton();
            this.rdbDezena3 = new System.Windows.Forms.RadioButton();
            this.rdbDezena9 = new System.Windows.Forms.RadioButton();
            this.rdbDezena4 = new System.Windows.Forms.RadioButton();
            this.rdbDezena6 = new System.Windows.Forms.RadioButton();
            this.rdbDezena1 = new System.Windows.Forms.RadioButton();
            this.grpUnidade = new System.Windows.Forms.GroupBox();
            this.rdbUnidade7 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade8 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade5 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade2 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade3 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade9 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade4 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade6 = new System.Windows.Forms.RadioButton();
            this.rdbUnidade1 = new System.Windows.Forms.RadioButton();
            this.lblResultado = new System.Windows.Forms.Label();
            this.grpMilhar.SuspendLayout();
            this.grpCentena.SuspendLayout();
            this.grpDezena.SuspendLayout();
            this.grpUnidade.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkMilhar
            // 
            this.chkMilhar.AutoSize = true;
            this.chkMilhar.Location = new System.Drawing.Point(12, 22);
            this.chkMilhar.Name = "chkMilhar";
            this.chkMilhar.Size = new System.Drawing.Size(54, 17);
            this.chkMilhar.TabIndex = 0;
            this.chkMilhar.Text = "Milhar";
            this.chkMilhar.UseVisualStyleBackColor = true;
            this.chkMilhar.CheckedChanged += new System.EventHandler(this.chkMilhar_CheckedChanged);
            // 
            // chkCentena
            // 
            this.chkCentena.AutoSize = true;
            this.chkCentena.Location = new System.Drawing.Point(153, 22);
            this.chkCentena.Name = "chkCentena";
            this.chkCentena.Size = new System.Drawing.Size(66, 17);
            this.chkCentena.TabIndex = 1;
            this.chkCentena.Text = "Centena";
            this.chkCentena.UseVisualStyleBackColor = true;
            this.chkCentena.CheckedChanged += new System.EventHandler(this.chkCentena_CheckedChanged);
            // 
            // chkDezena
            // 
            this.chkDezena.AutoSize = true;
            this.chkDezena.Location = new System.Drawing.Point(297, 22);
            this.chkDezena.Name = "chkDezena";
            this.chkDezena.Size = new System.Drawing.Size(63, 17);
            this.chkDezena.TabIndex = 2;
            this.chkDezena.Text = "Dezena";
            this.chkDezena.UseVisualStyleBackColor = true;
            this.chkDezena.CheckedChanged += new System.EventHandler(this.chkDezena_CheckedChanged);
            // 
            // chkUnidade
            // 
            this.chkUnidade.AutoSize = true;
            this.chkUnidade.Location = new System.Drawing.Point(448, 22);
            this.chkUnidade.Name = "chkUnidade";
            this.chkUnidade.Size = new System.Drawing.Size(66, 17);
            this.chkUnidade.TabIndex = 3;
            this.chkUnidade.Text = "Unidade";
            this.chkUnidade.UseVisualStyleBackColor = true;
            this.chkUnidade.CheckedChanged += new System.EventHandler(this.chkUnidade_CheckedChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(12, 339);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(113, 58);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // grpMilhar
            // 
            this.grpMilhar.Controls.Add(this.rdbMilhar7);
            this.grpMilhar.Controls.Add(this.rdbMilhar8);
            this.grpMilhar.Controls.Add(this.rdbMilhar5);
            this.grpMilhar.Controls.Add(this.rdbMilhar2);
            this.grpMilhar.Controls.Add(this.rdbMilhar3);
            this.grpMilhar.Controls.Add(this.rdbMilhar9);
            this.grpMilhar.Controls.Add(this.rdbMilhar4);
            this.grpMilhar.Controls.Add(this.rdbMilhar6);
            this.grpMilhar.Controls.Add(this.rdbMilhar1);
            this.grpMilhar.ForeColor = System.Drawing.Color.DodgerBlue;
            this.grpMilhar.Location = new System.Drawing.Point(12, 65);
            this.grpMilhar.Name = "grpMilhar";
            this.grpMilhar.Size = new System.Drawing.Size(113, 242);
            this.grpMilhar.TabIndex = 5;
            this.grpMilhar.TabStop = false;
            this.grpMilhar.Text = "Milhar";
            this.grpMilhar.Visible = false;
            // 
            // rdbMilhar7
            // 
            this.rdbMilhar7.AutoSize = true;
            this.rdbMilhar7.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar7.Location = new System.Drawing.Point(31, 157);
            this.rdbMilhar7.Name = "rdbMilhar7";
            this.rdbMilhar7.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar7.TabIndex = 26;
            this.rdbMilhar7.TabStop = true;
            this.rdbMilhar7.Text = "7";
            this.rdbMilhar7.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar8
            // 
            this.rdbMilhar8.AutoSize = true;
            this.rdbMilhar8.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar8.Location = new System.Drawing.Point(31, 179);
            this.rdbMilhar8.Name = "rdbMilhar8";
            this.rdbMilhar8.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar8.TabIndex = 25;
            this.rdbMilhar8.TabStop = true;
            this.rdbMilhar8.Text = "8";
            this.rdbMilhar8.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar5
            // 
            this.rdbMilhar5.AutoSize = true;
            this.rdbMilhar5.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar5.Location = new System.Drawing.Point(31, 114);
            this.rdbMilhar5.Name = "rdbMilhar5";
            this.rdbMilhar5.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar5.TabIndex = 24;
            this.rdbMilhar5.TabStop = true;
            this.rdbMilhar5.Text = "5";
            this.rdbMilhar5.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar2
            // 
            this.rdbMilhar2.AutoSize = true;
            this.rdbMilhar2.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar2.Location = new System.Drawing.Point(31, 45);
            this.rdbMilhar2.Name = "rdbMilhar2";
            this.rdbMilhar2.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar2.TabIndex = 23;
            this.rdbMilhar2.TabStop = true;
            this.rdbMilhar2.Text = "2";
            this.rdbMilhar2.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar3
            // 
            this.rdbMilhar3.AutoSize = true;
            this.rdbMilhar3.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar3.Location = new System.Drawing.Point(31, 68);
            this.rdbMilhar3.Name = "rdbMilhar3";
            this.rdbMilhar3.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar3.TabIndex = 22;
            this.rdbMilhar3.TabStop = true;
            this.rdbMilhar3.Text = "3";
            this.rdbMilhar3.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar9
            // 
            this.rdbMilhar9.AutoSize = true;
            this.rdbMilhar9.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar9.Location = new System.Drawing.Point(31, 203);
            this.rdbMilhar9.Name = "rdbMilhar9";
            this.rdbMilhar9.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar9.TabIndex = 21;
            this.rdbMilhar9.TabStop = true;
            this.rdbMilhar9.Text = "9";
            this.rdbMilhar9.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar4
            // 
            this.rdbMilhar4.AutoSize = true;
            this.rdbMilhar4.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar4.Location = new System.Drawing.Point(31, 91);
            this.rdbMilhar4.Name = "rdbMilhar4";
            this.rdbMilhar4.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar4.TabIndex = 20;
            this.rdbMilhar4.TabStop = true;
            this.rdbMilhar4.Text = "4";
            this.rdbMilhar4.UseVisualStyleBackColor = true;
            this.rdbMilhar4.CheckedChanged += new System.EventHandler(this.radioButton43_CheckedChanged);
            // 
            // rdbMilhar6
            // 
            this.rdbMilhar6.AutoSize = true;
            this.rdbMilhar6.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar6.Location = new System.Drawing.Point(31, 137);
            this.rdbMilhar6.Name = "rdbMilhar6";
            this.rdbMilhar6.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar6.TabIndex = 19;
            this.rdbMilhar6.TabStop = true;
            this.rdbMilhar6.Text = "6";
            this.rdbMilhar6.UseVisualStyleBackColor = true;
            // 
            // rdbMilhar1
            // 
            this.rdbMilhar1.AutoSize = true;
            this.rdbMilhar1.ForeColor = System.Drawing.Color.Black;
            this.rdbMilhar1.Location = new System.Drawing.Point(31, 22);
            this.rdbMilhar1.Name = "rdbMilhar1";
            this.rdbMilhar1.Size = new System.Drawing.Size(31, 17);
            this.rdbMilhar1.TabIndex = 18;
            this.rdbMilhar1.TabStop = true;
            this.rdbMilhar1.Text = "1";
            this.rdbMilhar1.UseVisualStyleBackColor = true;
            // 
            // grpCentena
            // 
            this.grpCentena.Controls.Add(this.rdbCentena7);
            this.grpCentena.Controls.Add(this.rdbCentena8);
            this.grpCentena.Controls.Add(this.rdbCentena5);
            this.grpCentena.Controls.Add(this.rdbCentena2);
            this.grpCentena.Controls.Add(this.rdbCentena3);
            this.grpCentena.Controls.Add(this.rdbCentena9);
            this.grpCentena.Controls.Add(this.rdbCentena4);
            this.grpCentena.Controls.Add(this.rdbCentena6);
            this.grpCentena.Controls.Add(this.rdbCentena1);
            this.grpCentena.ForeColor = System.Drawing.Color.DodgerBlue;
            this.grpCentena.Location = new System.Drawing.Point(153, 65);
            this.grpCentena.Name = "grpCentena";
            this.grpCentena.Size = new System.Drawing.Size(109, 242);
            this.grpCentena.TabIndex = 6;
            this.grpCentena.TabStop = false;
            this.grpCentena.Text = "Centena";
            this.grpCentena.Visible = false;
            // 
            // rdbCentena7
            // 
            this.rdbCentena7.AutoSize = true;
            this.rdbCentena7.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena7.Location = new System.Drawing.Point(27, 157);
            this.rdbCentena7.Name = "rdbCentena7";
            this.rdbCentena7.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena7.TabIndex = 35;
            this.rdbCentena7.TabStop = true;
            this.rdbCentena7.Text = "7";
            this.rdbCentena7.UseVisualStyleBackColor = true;
            // 
            // rdbCentena8
            // 
            this.rdbCentena8.AutoSize = true;
            this.rdbCentena8.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena8.Location = new System.Drawing.Point(27, 179);
            this.rdbCentena8.Name = "rdbCentena8";
            this.rdbCentena8.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena8.TabIndex = 34;
            this.rdbCentena8.TabStop = true;
            this.rdbCentena8.Text = "8";
            this.rdbCentena8.UseVisualStyleBackColor = true;
            // 
            // rdbCentena5
            // 
            this.rdbCentena5.AutoSize = true;
            this.rdbCentena5.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena5.Location = new System.Drawing.Point(27, 114);
            this.rdbCentena5.Name = "rdbCentena5";
            this.rdbCentena5.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena5.TabIndex = 33;
            this.rdbCentena5.TabStop = true;
            this.rdbCentena5.Text = "5";
            this.rdbCentena5.UseVisualStyleBackColor = true;
            // 
            // rdbCentena2
            // 
            this.rdbCentena2.AutoSize = true;
            this.rdbCentena2.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena2.Location = new System.Drawing.Point(27, 45);
            this.rdbCentena2.Name = "rdbCentena2";
            this.rdbCentena2.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena2.TabIndex = 32;
            this.rdbCentena2.TabStop = true;
            this.rdbCentena2.Text = "2";
            this.rdbCentena2.UseVisualStyleBackColor = true;
            // 
            // rdbCentena3
            // 
            this.rdbCentena3.AutoSize = true;
            this.rdbCentena3.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena3.Location = new System.Drawing.Point(27, 68);
            this.rdbCentena3.Name = "rdbCentena3";
            this.rdbCentena3.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena3.TabIndex = 31;
            this.rdbCentena3.TabStop = true;
            this.rdbCentena3.Text = "3";
            this.rdbCentena3.UseVisualStyleBackColor = true;
            // 
            // rdbCentena9
            // 
            this.rdbCentena9.AutoSize = true;
            this.rdbCentena9.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena9.Location = new System.Drawing.Point(27, 203);
            this.rdbCentena9.Name = "rdbCentena9";
            this.rdbCentena9.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena9.TabIndex = 30;
            this.rdbCentena9.TabStop = true;
            this.rdbCentena9.Text = "9";
            this.rdbCentena9.UseVisualStyleBackColor = true;
            // 
            // rdbCentena4
            // 
            this.rdbCentena4.AutoSize = true;
            this.rdbCentena4.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena4.Location = new System.Drawing.Point(27, 91);
            this.rdbCentena4.Name = "rdbCentena4";
            this.rdbCentena4.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena4.TabIndex = 29;
            this.rdbCentena4.TabStop = true;
            this.rdbCentena4.Text = "4";
            this.rdbCentena4.UseVisualStyleBackColor = true;
            // 
            // rdbCentena6
            // 
            this.rdbCentena6.AutoSize = true;
            this.rdbCentena6.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena6.Location = new System.Drawing.Point(27, 137);
            this.rdbCentena6.Name = "rdbCentena6";
            this.rdbCentena6.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena6.TabIndex = 28;
            this.rdbCentena6.TabStop = true;
            this.rdbCentena6.Text = "6";
            this.rdbCentena6.UseVisualStyleBackColor = true;
            // 
            // rdbCentena1
            // 
            this.rdbCentena1.AutoSize = true;
            this.rdbCentena1.ForeColor = System.Drawing.Color.Black;
            this.rdbCentena1.Location = new System.Drawing.Point(27, 22);
            this.rdbCentena1.Name = "rdbCentena1";
            this.rdbCentena1.Size = new System.Drawing.Size(31, 17);
            this.rdbCentena1.TabIndex = 27;
            this.rdbCentena1.TabStop = true;
            this.rdbCentena1.Text = "1";
            this.rdbCentena1.UseVisualStyleBackColor = true;
            // 
            // grpDezena
            // 
            this.grpDezena.Controls.Add(this.rdbDezena7);
            this.grpDezena.Controls.Add(this.rdbDezena8);
            this.grpDezena.Controls.Add(this.rdbDezena5);
            this.grpDezena.Controls.Add(this.rdbDezena2);
            this.grpDezena.Controls.Add(this.rdbDezena3);
            this.grpDezena.Controls.Add(this.rdbDezena9);
            this.grpDezena.Controls.Add(this.rdbDezena4);
            this.grpDezena.Controls.Add(this.rdbDezena6);
            this.grpDezena.Controls.Add(this.rdbDezena1);
            this.grpDezena.ForeColor = System.Drawing.Color.DodgerBlue;
            this.grpDezena.Location = new System.Drawing.Point(295, 65);
            this.grpDezena.Name = "grpDezena";
            this.grpDezena.Size = new System.Drawing.Size(117, 242);
            this.grpDezena.TabIndex = 7;
            this.grpDezena.TabStop = false;
            this.grpDezena.Text = "Dezena";
            this.grpDezena.Visible = false;
            this.grpDezena.Enter += new System.EventHandler(this.grpDezena_Enter);
            // 
            // rdbDezena7
            // 
            this.rdbDezena7.AutoSize = true;
            this.rdbDezena7.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena7.Location = new System.Drawing.Point(34, 157);
            this.rdbDezena7.Name = "rdbDezena7";
            this.rdbDezena7.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena7.TabIndex = 35;
            this.rdbDezena7.TabStop = true;
            this.rdbDezena7.Text = "7";
            this.rdbDezena7.UseVisualStyleBackColor = true;
            // 
            // rdbDezena8
            // 
            this.rdbDezena8.AutoSize = true;
            this.rdbDezena8.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena8.Location = new System.Drawing.Point(34, 179);
            this.rdbDezena8.Name = "rdbDezena8";
            this.rdbDezena8.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena8.TabIndex = 34;
            this.rdbDezena8.TabStop = true;
            this.rdbDezena8.Text = "8";
            this.rdbDezena8.UseVisualStyleBackColor = true;
            // 
            // rdbDezena5
            // 
            this.rdbDezena5.AutoSize = true;
            this.rdbDezena5.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena5.Location = new System.Drawing.Point(34, 114);
            this.rdbDezena5.Name = "rdbDezena5";
            this.rdbDezena5.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena5.TabIndex = 33;
            this.rdbDezena5.TabStop = true;
            this.rdbDezena5.Text = "5";
            this.rdbDezena5.UseVisualStyleBackColor = true;
            // 
            // rdbDezena2
            // 
            this.rdbDezena2.AutoSize = true;
            this.rdbDezena2.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena2.Location = new System.Drawing.Point(34, 45);
            this.rdbDezena2.Name = "rdbDezena2";
            this.rdbDezena2.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena2.TabIndex = 32;
            this.rdbDezena2.TabStop = true;
            this.rdbDezena2.Text = "2";
            this.rdbDezena2.UseVisualStyleBackColor = true;
            // 
            // rdbDezena3
            // 
            this.rdbDezena3.AutoSize = true;
            this.rdbDezena3.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena3.Location = new System.Drawing.Point(34, 68);
            this.rdbDezena3.Name = "rdbDezena3";
            this.rdbDezena3.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena3.TabIndex = 31;
            this.rdbDezena3.TabStop = true;
            this.rdbDezena3.Text = "3";
            this.rdbDezena3.UseVisualStyleBackColor = true;
            // 
            // rdbDezena9
            // 
            this.rdbDezena9.AutoSize = true;
            this.rdbDezena9.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena9.Location = new System.Drawing.Point(34, 203);
            this.rdbDezena9.Name = "rdbDezena9";
            this.rdbDezena9.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena9.TabIndex = 30;
            this.rdbDezena9.TabStop = true;
            this.rdbDezena9.Text = "9";
            this.rdbDezena9.UseVisualStyleBackColor = true;
            // 
            // rdbDezena4
            // 
            this.rdbDezena4.AutoSize = true;
            this.rdbDezena4.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena4.Location = new System.Drawing.Point(34, 91);
            this.rdbDezena4.Name = "rdbDezena4";
            this.rdbDezena4.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena4.TabIndex = 29;
            this.rdbDezena4.TabStop = true;
            this.rdbDezena4.Text = "4";
            this.rdbDezena4.UseVisualStyleBackColor = true;
            // 
            // rdbDezena6
            // 
            this.rdbDezena6.AutoSize = true;
            this.rdbDezena6.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena6.Location = new System.Drawing.Point(34, 137);
            this.rdbDezena6.Name = "rdbDezena6";
            this.rdbDezena6.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena6.TabIndex = 28;
            this.rdbDezena6.TabStop = true;
            this.rdbDezena6.Text = "6";
            this.rdbDezena6.UseVisualStyleBackColor = true;
            // 
            // rdbDezena1
            // 
            this.rdbDezena1.AutoSize = true;
            this.rdbDezena1.ForeColor = System.Drawing.Color.Black;
            this.rdbDezena1.Location = new System.Drawing.Point(34, 22);
            this.rdbDezena1.Name = "rdbDezena1";
            this.rdbDezena1.Size = new System.Drawing.Size(31, 17);
            this.rdbDezena1.TabIndex = 27;
            this.rdbDezena1.TabStop = true;
            this.rdbDezena1.Text = "1";
            this.rdbDezena1.UseVisualStyleBackColor = true;
            // 
            // grpUnidade
            // 
            this.grpUnidade.Controls.Add(this.rdbUnidade7);
            this.grpUnidade.Controls.Add(this.rdbUnidade8);
            this.grpUnidade.Controls.Add(this.rdbUnidade5);
            this.grpUnidade.Controls.Add(this.rdbUnidade2);
            this.grpUnidade.Controls.Add(this.rdbUnidade3);
            this.grpUnidade.Controls.Add(this.rdbUnidade9);
            this.grpUnidade.Controls.Add(this.rdbUnidade4);
            this.grpUnidade.Controls.Add(this.rdbUnidade6);
            this.grpUnidade.Controls.Add(this.rdbUnidade1);
            this.grpUnidade.ForeColor = System.Drawing.Color.DodgerBlue;
            this.grpUnidade.Location = new System.Drawing.Point(449, 65);
            this.grpUnidade.Name = "grpUnidade";
            this.grpUnidade.Size = new System.Drawing.Size(118, 242);
            this.grpUnidade.TabIndex = 8;
            this.grpUnidade.TabStop = false;
            this.grpUnidade.Text = "Unidade";
            this.grpUnidade.Visible = false;
            // 
            // rdbUnidade7
            // 
            this.rdbUnidade7.AutoSize = true;
            this.rdbUnidade7.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade7.Location = new System.Drawing.Point(34, 157);
            this.rdbUnidade7.Name = "rdbUnidade7";
            this.rdbUnidade7.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade7.TabIndex = 35;
            this.rdbUnidade7.TabStop = true;
            this.rdbUnidade7.Text = "7";
            this.rdbUnidade7.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade8
            // 
            this.rdbUnidade8.AutoSize = true;
            this.rdbUnidade8.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade8.Location = new System.Drawing.Point(34, 179);
            this.rdbUnidade8.Name = "rdbUnidade8";
            this.rdbUnidade8.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade8.TabIndex = 34;
            this.rdbUnidade8.TabStop = true;
            this.rdbUnidade8.Text = "8";
            this.rdbUnidade8.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade5
            // 
            this.rdbUnidade5.AutoSize = true;
            this.rdbUnidade5.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade5.Location = new System.Drawing.Point(34, 114);
            this.rdbUnidade5.Name = "rdbUnidade5";
            this.rdbUnidade5.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade5.TabIndex = 33;
            this.rdbUnidade5.TabStop = true;
            this.rdbUnidade5.Text = "5";
            this.rdbUnidade5.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade2
            // 
            this.rdbUnidade2.AutoSize = true;
            this.rdbUnidade2.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade2.Location = new System.Drawing.Point(34, 45);
            this.rdbUnidade2.Name = "rdbUnidade2";
            this.rdbUnidade2.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade2.TabIndex = 32;
            this.rdbUnidade2.TabStop = true;
            this.rdbUnidade2.Text = "2";
            this.rdbUnidade2.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade3
            // 
            this.rdbUnidade3.AutoSize = true;
            this.rdbUnidade3.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade3.Location = new System.Drawing.Point(34, 68);
            this.rdbUnidade3.Name = "rdbUnidade3";
            this.rdbUnidade3.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade3.TabIndex = 31;
            this.rdbUnidade3.TabStop = true;
            this.rdbUnidade3.Text = "3";
            this.rdbUnidade3.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade9
            // 
            this.rdbUnidade9.AutoSize = true;
            this.rdbUnidade9.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade9.Location = new System.Drawing.Point(34, 203);
            this.rdbUnidade9.Name = "rdbUnidade9";
            this.rdbUnidade9.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade9.TabIndex = 30;
            this.rdbUnidade9.TabStop = true;
            this.rdbUnidade9.Text = "9";
            this.rdbUnidade9.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade4
            // 
            this.rdbUnidade4.AutoSize = true;
            this.rdbUnidade4.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade4.Location = new System.Drawing.Point(34, 91);
            this.rdbUnidade4.Name = "rdbUnidade4";
            this.rdbUnidade4.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade4.TabIndex = 29;
            this.rdbUnidade4.TabStop = true;
            this.rdbUnidade4.Text = "4";
            this.rdbUnidade4.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade6
            // 
            this.rdbUnidade6.AutoSize = true;
            this.rdbUnidade6.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade6.Location = new System.Drawing.Point(34, 137);
            this.rdbUnidade6.Name = "rdbUnidade6";
            this.rdbUnidade6.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade6.TabIndex = 28;
            this.rdbUnidade6.TabStop = true;
            this.rdbUnidade6.Text = "6";
            this.rdbUnidade6.UseVisualStyleBackColor = true;
            // 
            // rdbUnidade1
            // 
            this.rdbUnidade1.AutoSize = true;
            this.rdbUnidade1.ForeColor = System.Drawing.Color.Black;
            this.rdbUnidade1.Location = new System.Drawing.Point(34, 22);
            this.rdbUnidade1.Name = "rdbUnidade1";
            this.rdbUnidade1.Size = new System.Drawing.Size(31, 17);
            this.rdbUnidade1.TabIndex = 27;
            this.rdbUnidade1.TabStop = true;
            this.rdbUnidade1.Text = "1";
            this.rdbUnidade1.UseVisualStyleBackColor = true;
            // 
            // lblResultado
            // 
            this.lblResultado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResultado.Location = new System.Drawing.Point(142, 339);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(406, 58);
            this.lblResultado.TabIndex = 9;
            // 
            // Exemplo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 438);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.grpUnidade);
            this.Controls.Add(this.grpDezena);
            this.Controls.Add(this.grpCentena);
            this.Controls.Add(this.grpMilhar);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chkMilhar);
            this.Controls.Add(this.chkCentena);
            this.Controls.Add(this.chkDezena);
            this.Controls.Add(this.chkUnidade);
            this.Name = "Exemplo";
            this.Text = "DS1 - AULA 06";
            this.Load += new System.EventHandler(this.Exemplo_Load);
            this.grpMilhar.ResumeLayout(false);
            this.grpMilhar.PerformLayout();
            this.grpCentena.ResumeLayout(false);
            this.grpCentena.PerformLayout();
            this.grpDezena.ResumeLayout(false);
            this.grpDezena.PerformLayout();
            this.grpUnidade.ResumeLayout(false);
            this.grpUnidade.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkUnidade;
        private System.Windows.Forms.CheckBox chkDezena;
        private System.Windows.Forms.CheckBox chkCentena;
        private System.Windows.Forms.CheckBox chkMilhar;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox grpMilhar;
        private System.Windows.Forms.RadioButton rdbMilhar7;
        private System.Windows.Forms.RadioButton rdbMilhar8;
        private System.Windows.Forms.RadioButton rdbMilhar5;
        private System.Windows.Forms.RadioButton rdbMilhar2;
        private System.Windows.Forms.RadioButton rdbMilhar3;
        private System.Windows.Forms.RadioButton rdbMilhar9;
        private System.Windows.Forms.RadioButton rdbMilhar4;
        private System.Windows.Forms.RadioButton rdbMilhar6;
        private System.Windows.Forms.RadioButton rdbMilhar1;
        private System.Windows.Forms.GroupBox grpCentena;
        private System.Windows.Forms.RadioButton rdbCentena7;
        private System.Windows.Forms.RadioButton rdbCentena8;
        private System.Windows.Forms.RadioButton rdbCentena5;
        private System.Windows.Forms.RadioButton rdbCentena2;
        private System.Windows.Forms.RadioButton rdbCentena3;
        private System.Windows.Forms.RadioButton rdbCentena9;
        private System.Windows.Forms.RadioButton rdbCentena4;
        private System.Windows.Forms.RadioButton rdbCentena6;
        private System.Windows.Forms.RadioButton rdbCentena1;
        private System.Windows.Forms.GroupBox grpDezena;
        private System.Windows.Forms.RadioButton rdbDezena7;
        private System.Windows.Forms.RadioButton rdbDezena8;
        private System.Windows.Forms.RadioButton rdbDezena5;
        private System.Windows.Forms.RadioButton rdbDezena2;
        private System.Windows.Forms.RadioButton rdbDezena3;
        private System.Windows.Forms.RadioButton rdbDezena9;
        private System.Windows.Forms.RadioButton rdbDezena4;
        private System.Windows.Forms.RadioButton rdbDezena6;
        private System.Windows.Forms.RadioButton rdbDezena1;
        private System.Windows.Forms.GroupBox grpUnidade;
        private System.Windows.Forms.RadioButton rdbUnidade7;
        private System.Windows.Forms.RadioButton rdbUnidade8;
        private System.Windows.Forms.RadioButton rdbUnidade5;
        private System.Windows.Forms.RadioButton rdbUnidade2;
        private System.Windows.Forms.RadioButton rdbUnidade3;
        private System.Windows.Forms.RadioButton rdbUnidade9;
        private System.Windows.Forms.RadioButton rdbUnidade4;
        private System.Windows.Forms.RadioButton rdbUnidade6;
        private System.Windows.Forms.RadioButton rdbUnidade1;
        private System.Windows.Forms.Label lblResultado;
    }
}

