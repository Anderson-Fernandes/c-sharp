﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LISTA2_EXE10_ALCF_ERE
{
    public partial class frm_exe10 : Form
    {
        public frm_exe10()
        {
            InitializeComponent();
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void rdb_feminino_CheckedChanged(object sender, EventArgs e)
        {
            double pesoAE, alturaAE;
            alturaAE = double.Parse(txt_altura.Text);
            pesoAE = (62.7 * alturaAE) - 44.7;
            pesoAE.ToString();
            lbl_peso.Text = "Seu peso ideal é " + pesoAE;
        }

        private void rdb_masculino_CheckedChanged(object sender, EventArgs e)
        {
            double pesoAE, alturaAE;
            alturaAE = double.Parse(txt_altura.Text);
            pesoAE = (72.2 * alturaAE) - 58;
            pesoAE.ToString();
            lbl_peso.Text = "Seu peso ideal é " + pesoAE;
        }
    }
}
