﻿namespace LISTA2_EXE9_ALCF_ERE
{
    partial class FRM_EXE9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_cota = new System.Windows.Forms.Label();
            this.lbl_dolar = new System.Windows.Forms.Label();
            this.txt_dolar = new System.Windows.Forms.TextBox();
            this.txt_cota = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_cota
            // 
            this.lbl_cota.AutoSize = true;
            this.lbl_cota.Location = new System.Drawing.Point(12, 77);
            this.lbl_cota.Name = "lbl_cota";
            this.lbl_cota.Size = new System.Drawing.Size(88, 13);
            this.lbl_cota.TabIndex = 0;
            this.lbl_cota.Text = "Digite a cotação:";
            this.lbl_cota.Click += new System.EventHandler(this.lbl_cota_Click);
            // 
            // lbl_dolar
            // 
            this.lbl_dolar.AutoSize = true;
            this.lbl_dolar.Location = new System.Drawing.Point(12, 33);
            this.lbl_dolar.Name = "lbl_dolar";
            this.lbl_dolar.Size = new System.Drawing.Size(72, 13);
            this.lbl_dolar.TabIndex = 1;
            this.lbl_dolar.Text = "Digite o dolar:";
            this.lbl_dolar.Click += new System.EventHandler(this.lbl_dolar_Click);
            // 
            // txt_dolar
            // 
            this.txt_dolar.Location = new System.Drawing.Point(146, 30);
            this.txt_dolar.Name = "txt_dolar";
            this.txt_dolar.Size = new System.Drawing.Size(119, 20);
            this.txt_dolar.TabIndex = 2;
            this.txt_dolar.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txt_cota
            // 
            this.txt_cota.Location = new System.Drawing.Point(146, 74);
            this.txt_cota.Name = "txt_cota";
            this.txt_cota.Size = new System.Drawing.Size(119, 20);
            this.txt_cota.TabIndex = 3;
            this.txt_cota.TextChanged += new System.EventHandler(this.txt_cota_TextChanged);
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(107, 209);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 4;
            this.btn_calc.Text = "Calcular";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // FRM_EXE9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_cota);
            this.Controls.Add(this.txt_dolar);
            this.Controls.Add(this.lbl_dolar);
            this.Controls.Add(this.lbl_cota);
            this.Name = "FRM_EXE9";
            this.ShowIcon = false;
            this.Text = "Dolar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_cota;
        private System.Windows.Forms.Label lbl_dolar;
        private System.Windows.Forms.TextBox txt_dolar;
        private System.Windows.Forms.TextBox txt_cota;
        private System.Windows.Forms.Button btn_calc;
    }
}

