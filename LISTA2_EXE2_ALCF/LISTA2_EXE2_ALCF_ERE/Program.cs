﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LISTA2_EXE2_ALCF_ERE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Frm_livraria());
        }
    }
}
