        private void Form3_Load(object sender, EventArgs e)
        {
            MySqlConnection conectaBanco = new MySqlConnection("server=localhost;user id=root;password=root;persistsecurityinfo=True;database=db_escola");
            MySqlCommand comandoLerDados = new MySqlCommand("select cd_estado, sg_estado, nm_estado from tb_estado", conectaBanco);
            DataTable tabelaDadosMemoria = new DataTable();
            MySqlDataAdapter adaptador = new MySqlDataAdapter();
            adaptador.SelectCommand = comandoLerDados;
            conectaBanco.Open();
            adaptador.Fill(tabelaDadosMemoria);
            conectaBanco.Close();
            listBox1.DataSource = tabelaDadosMemoria;
            listBox1.DisplayMember="nm_estado";
            listBox1.ValueMember = "sg_estado";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlConnection conectaBanco = new MySqlConnection("server=localhost;user id=root;password=root;persistsecurityinfo=True;database=db_escola");
            MySqlCommand comandoLerDados = new MySqlCommand();
            MySqlDataReader dr;
            comandoLerDados.Connection = conectaBanco;
            comandoLerDados.CommandText = "select cd_estado, sg_estado, nm_estado from tb_estado WHERE sg_estado = '" + listBox1.SelectedValue + "'";
            conectaBanco.Open();
            dr = comandoLerDados.ExecuteReader();
            while (dr.Read())
            {
                textBox1.Text = dr.GetString(1);
                pictureBox1.Image = Image.FromFile(@"C:\Mapas\" + dr.GetString(0) + ".png");
            }
            conectaBanco.Close();
        }
    }
}