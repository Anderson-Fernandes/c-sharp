﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace supermercado
{
    class conDB
    {
        MySqlConnection conexao = new MySqlConnection("server=localhost;user id=root;password=root;persistsecurityinfo=True;database=db_supermercado"
        DataTable dt = new DataTable();
        MySqlDataAdapter da = new MySqlDataAdapter();

        public DataTable select(String campo, String tabela)
        {
            MySqlCommand linha = new MySqlCommand("SELECT " + campo + " from " + tabela + ";", conexao);
            da.SelectCommand = linha;
            finish(dt);
            return dt;
        }

        public DataTable select(String campo, String tabela, String condicao)
        {
            DataTable busca = new DataTable();
            MySqlCommand linha = new MySqlCommand("SELECT " + campo + " from " + tabela + " WHERE " + condicao + " ;", conexao);
            da.SelectCommand = linha;
            finish(busca);
            return busca;
        }
    }
}