﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LISTA1_EXE2_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            float notaA = 0;
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Digite a {0}° Nota: ", (i+1));
                if ((i + 1) <= 2)
                {
                    notaA += (float.Parse(Console.ReadLine())) * 2;
                }
                else
                {
                    notaA += (float.Parse(Console.ReadLine())) * 3;
                }
            }
            notaA = notaA / 10;
            Console.WriteLine("A Media Ponderada é: {0}",notaA);
            Console.ReadKey();
        }
    }
}
