﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lista3_exe1_ALCF_ERE
{
    public partial class frm_EXE1 : Form
    {
        public frm_EXE1()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (txt_num1.TextLength == 0 || txt_num2.TextLength == 0)
            {
                MessageBox.Show("Por Favor, Preencha todos os campos");
            }
            else
            {
                try
                {
                    int difAE = 0;
                    if (int.Parse(txt_num1.Text) == int.Parse(txt_num2.Text))
                    {
                        difAE = int.Parse(txt_num1.Text) + int.Parse(txt_num2.Text);
                        MessageBox.Show("A soma dos numeros \n" + difAE, "OK", MessageBoxButtons.OK);
                    }
                    else if (int.Parse(txt_num1.Text) > int.Parse(txt_num2.Text))
                    {
                        difAE = int.Parse(txt_num1.Text) - int.Parse(txt_num2.Text);
                        MessageBox.Show("A diferença entre os numeros \n" + difAE, "Resultado", MessageBoxButtons.OK);
                    }
                    else
                    {
                        difAE = int.Parse(txt_num2.Text) - int.Parse(txt_num1.Text);
                        MessageBox.Show("A diferença entre os numeros \n" + difAE, "Resultado", MessageBoxButtons.OK);
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Digite apenas numeros");
                }
                catch (OverflowException)
                {
                    MessageBox.Show("Digite numero menor");
                }

            }
        }

        private void btn_Sair_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Deseja Sair?","Sair",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void txt_num1_TextChanged(object sender, EventArgs e)
        {
            SendKeys.Send("{ENTER}");
        }
        

    }
}
