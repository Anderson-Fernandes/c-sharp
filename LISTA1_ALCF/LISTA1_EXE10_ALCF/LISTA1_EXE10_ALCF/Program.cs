﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LISTA1_EXE10_ALCF
{
    class Program
    {
        static void Main(string[] args)
        {
            //4*3.14*N³/3
            Console.WriteLine("Entre com o Raio: ");
            double raioA = double.Parse(Console.ReadLine());
            raioA = 4 * Math.PI* ((raioA) * (raioA) * (raioA)) / 3;
            Console.WriteLine("O volume é: {0}", raioA);
            Console.ReadKey();
        }
    }
}
