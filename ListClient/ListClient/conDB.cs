﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace ListClient
{
    class conDB
    {
        MySqlConnection conexao = new MySqlConnection("server=localhost;user id=root;password=root;persistsecurityinfo=True;database=db_test");
        DataTable dt = new DataTable();
        MySqlDataAdapter da = new MySqlDataAdapter();

       public DataTable select(String campo, String tabela){
            MySqlCommand linha = new MySqlCommand("SELECT " + campo +" from "+ tabela+";", conexao);
            da.SelectCommand = linha;
            finish(dt);
           return dt;
        }

       public DataTable select(String campo, String tabela, String condicao)
       {
           DataTable busca = new DataTable();
           MySqlCommand linha = new MySqlCommand("SELECT " + campo + " from " + tabela + " WHERE "+ condicao +" ;", conexao);
           da.SelectCommand = linha;
           finish(busca);
           return busca;
       }




       private void finish(DataTable tabela){
           conexao.Open();
           da.Fill(tabela);
           conexao.Close();
       }

       public String retorno(ListBox list, String campo)
       {
           list.ValueMember = campo;
           String a =""+   list.SelectedValue;
           return a;
       }
    }
}
