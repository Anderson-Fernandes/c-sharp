﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aula_12
{
    class Program
    {
        static void Main(string[] args)
        {
            String alternativas;
            int opcao;

            Console.WriteLine("Bem Vindo! Entenda como funciona um Try/Catch/Finally");

            Console.WriteLine("1 - Sem Try/Catch/Finally");
            Console.WriteLine("2 - Try/Catch");
            Console.WriteLine("3 - Try/Catch/Finally (Exceção Genérica");
            Console.WriteLine("4 - Try/Catch/Finally (Exceção Variada");

            Console.Write("Escolha a opção desejada: ");

            alternativas = Console.ReadLine();

            Console.WriteLine();

            opcao = int.Parse(alternativas);

            switch (opcao)
            {
                case 1:
                    metodo1();
                    break;
                case 2:
                    metodo2();
                    break;
                case 3:
                    metodo3();
                    break;
                case 4:
                    metodo4();
                    break;
                default:
                    Console.WriteLine("Não existe essa opção! Reinicie a aplicação.");
                    Console.ReadKey();
                    break;
            }
        }

        private static void metodo1()
        {
           //Neste método irá acontecer um erro/exceção, pois como sabemos não existe divisão por 0
            // o programa será encerrado e aparcerá um mensagem mostrando o erro e o local
            // dele no código.

            Console.WriteLine("Iremos dividir 10 por 0 para ver o que acontecer: Pressione <enter>");
            Console.ReadKey();

            int num1, num2, resp;

            num1 = 10;
            num2 = 0;

            //Está linha gerará o erro/exceção
            resp = num1 / num2;

            Console.ReadKey();
        }

        private static void metodo2()
        {
            // Código que pode gerar um erro/exceção

            try
            {
                Console.WriteLine("Iremos dividir 10 por 0 para ver o que acontecer: Pressione <enter>");
                Console.ReadKey();

                int num1, num2, resp;

                num1 = 10;
                num2 = 0;

                //Está linha gerará o erro/exceção
                resp = num1 / num2;
            }
            //Código para tratamento de erros
            catch (Exception ex)
            {
                Console.WriteLine("Impossivel Dividir por zero!");
                //Código para tratamento de erros

                Console.ReadKey();
            }
        }

        private static void metodo3()
        {
             // Código que pode gerar um erro/exceção

            try
            {
                Console.WriteLine("Iremos dividir 10 por 0 para ver o que acontecer: Pressione <enter>");
                Console.ReadKey();

                int num1, num2, resp;

                num1 = 10;
                num2 = 0;

                //Está linha gerará o erro/exceção
                resp = num1 / num2;
            }
            //Código para tratamento de erros
            catch (Exception ex)
            {
                Console.WriteLine("Erros: " + ex.Message);
                //Código de execução Obrigatoria
            }

            Console.ReadKey();
        }

        private static void metodo4()
        {
            // Código que pode gerar um erro/exceção

            try
            {
                Console.WriteLine("Iremos dividir 10 por 0 para ver o que acontecer: Pressione <enter>");
                Console.ReadKey();

                int num1, num2, resp;

                num1 = 10;
                num2 = 0;

                //Está linha gerará o erro/exceção
                resp = num1 / num2;
            }
            //Código para tratamento de erros
            catch (DivideByZeroException ex)
            {
                Console.WriteLine("O erro ocorreu por que não existe divisão por zero (0). " + ex.Message);
                //Codigo para tratamento de erros
            }
            catch(OverflowException ex){
                Console.WriteLine("Erros: " + ex.Message);
                //Código de execução Obrigatoria
            }

            Console.ReadKey();
        }
    }
}
