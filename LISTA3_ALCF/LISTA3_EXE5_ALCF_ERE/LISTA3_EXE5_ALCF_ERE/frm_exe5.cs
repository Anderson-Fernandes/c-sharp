﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LISTA3_EXE5_ALCF_ERE
{
    public partial class frm_exe5 : Form
    {
        public frm_exe5()
        {
            InitializeComponent();
        }
        private void Sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Sair? ", "Sair", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (txt_salario.TextLength < 1)
            {
                MessageBox.Show("Preencha o campo", "ATENÇÃO!", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    double salarioAE, reajusteAE, salariorAE;
                    salarioAE = double.Parse(txt_salario.Text);
                    if (salarioAE < 500.00)
                    {
                        reajusteAE = salarioAE * 0.15;
                        salariorAE = salarioAE + reajusteAE;
                        MessageBox.Show("O salário com reajuste é: " + salariorAE.ToString("C"), "Salário", MessageBoxButtons.OK);
                    }
                    else if (salarioAE >= 500.00 && salarioAE <= 1000.00)
                    {
                        reajusteAE = salarioAE * 0.10;
                        salariorAE = salarioAE + reajusteAE;
                        MessageBox.Show("O salário com reajuste é: " + salariorAE.ToString("C"), "Salário", MessageBoxButtons.OK);
                    }
                    else
                    {
                        reajusteAE = salarioAE * 0.05;
                        salariorAE = salarioAE + reajusteAE;
                        MessageBox.Show("O salário com reajuste é: " + salariorAE.ToString("C"), "Salário", MessageBoxButtons.OK);
                    }
                }
                catch(FormatException)
                {
                    MessageBox.Show("Digite apenas numeros","ATENÇÃO!",MessageBoxButtons.OK);
                }
                catch(OverflowException)
                {
                    MessageBox.Show("Digite um numero menor","ATEÇÃO!",MessageBoxButtons.OK);
                }
            }
        }
    }
}

