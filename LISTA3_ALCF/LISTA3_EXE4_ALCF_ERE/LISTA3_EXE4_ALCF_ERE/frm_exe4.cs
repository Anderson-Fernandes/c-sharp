﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LISTA3_EXE4_ALCF_ERE
{
    public partial class frm_exe4 : Form
    {
        public frm_exe4()
        {
            InitializeComponent();
        }

        private void btn_entrar_Click(object sender, EventArgs e)
        {
            if (txt_num.TextLength == 0)
            {
                MessageBox.Show("Por favor, Preencha todos os campos!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    if (int.Parse(txt_num.Text) > 0)
                    {
                        lbl_tab.Text = "";
                        int tabAE = int.Parse(txt_num.Text);
                        for (int c = 0; c <= 10; c++)
                        {
                            int res = tabAE * c;
                            lbl_tab.Text += tabAE + " X " + c + " = " + res + "\n";
                        }
                    }
                    else
                    {
                        MessageBox.Show("Por favor, Digite um numero maior que Zero", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Por favor, Digite apenas numeros!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (OverflowException)
                {
                    MessageBox.Show("Por favor, Digite um numero menor!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
