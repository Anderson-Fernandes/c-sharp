﻿namespace LISTA3_EXE7_ALCF_ERE
{
    partial class frm_exe7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_ht = new System.Windows.Forms.TextBox();
            this.lbl_horastrabalhadas = new System.Windows.Forms.Label();
            this.txt_vh = new System.Windows.Forms.TextBox();
            this.lbl_valorhora = new System.Windows.Forms.Label();
            this.btn_calcular = new System.Windows.Forms.Button();
            this.btn_sair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_ht
            // 
            this.txt_ht.Location = new System.Drawing.Point(228, 17);
            this.txt_ht.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_ht.Name = "txt_ht";
            this.txt_ht.Size = new System.Drawing.Size(183, 20);
            this.txt_ht.TabIndex = 10;
            // 
            // lbl_horastrabalhadas
            // 
            this.lbl_horastrabalhadas.AutoSize = true;
            this.lbl_horastrabalhadas.Location = new System.Drawing.Point(12, 20);
            this.lbl_horastrabalhadas.Name = "lbl_horastrabalhadas";
            this.lbl_horastrabalhadas.Size = new System.Drawing.Size(204, 13);
            this.lbl_horastrabalhadas.TabIndex = 9;
            this.lbl_horastrabalhadas.Text = "Digite a quantidade de horas trabalhadas:";
            // 
            // txt_vh
            // 
            this.txt_vh.Location = new System.Drawing.Point(228, 45);
            this.txt_vh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_vh.Name = "txt_vh";
            this.txt_vh.Size = new System.Drawing.Size(183, 20);
            this.txt_vh.TabIndex = 12;
            // 
            // lbl_valorhora
            // 
            this.lbl_valorhora.AutoSize = true;
            this.lbl_valorhora.Location = new System.Drawing.Point(12, 48);
            this.lbl_valorhora.Name = "lbl_valorhora";
            this.lbl_valorhora.Size = new System.Drawing.Size(167, 13);
            this.lbl_valorhora.TabIndex = 11;
            this.lbl_valorhora.Text = "Digite o valor da hora trabalhada: ";
            // 
            // btn_calcular
            // 
            this.btn_calcular.Location = new System.Drawing.Point(336, 84);
            this.btn_calcular.Name = "btn_calcular";
            this.btn_calcular.Size = new System.Drawing.Size(75, 23);
            this.btn_calcular.TabIndex = 13;
            this.btn_calcular.Text = "Calcular";
            this.btn_calcular.UseVisualStyleBackColor = true;
            this.btn_calcular.Click += new System.EventHandler(this.btn_calcular_Click);
            // 
            // btn_sair
            // 
            this.btn_sair.Location = new System.Drawing.Point(37, 84);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(75, 23);
            this.btn_sair.TabIndex = 14;
            this.btn_sair.Text = "&Sair";
            this.btn_sair.UseVisualStyleBackColor = true;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // frm_exe7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 132);
            this.ControlBox = false;
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.btn_calcular);
            this.Controls.Add(this.txt_vh);
            this.Controls.Add(this.lbl_valorhora);
            this.Controls.Add(this.txt_ht);
            this.Controls.Add(this.lbl_horastrabalhadas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frm_exe7";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Horas Trabalhadas";
            this.Load += new System.EventHandler(this.frm_exe7_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_ht;
        private System.Windows.Forms.Label lbl_horastrabalhadas;
        private System.Windows.Forms.TextBox txt_vh;
        private System.Windows.Forms.Label lbl_valorhora;
        private System.Windows.Forms.Button btn_calcular;
        private System.Windows.Forms.Button btn_sair;
    }
}

